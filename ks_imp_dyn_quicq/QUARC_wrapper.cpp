#include "QUARC_wrapper.h"
#include <quarc.hpp>

void initQuarc (int *argc, char ***argv)
{
  quarc::metl::quarc_init(argc, argv);
}

void finalizeQuarc ()
{
  quarc::metl::quarc_finalize();
}
