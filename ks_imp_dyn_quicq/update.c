/********** update.c ****************************************************/
/* MIMD version 7 */

/*
 Update lattice.
 Improved method for 1-4 flavors:
 update U by (epsilon/2)*(1-Nf/4)
 compute PHI
 update U to epsilon/2
 compute X
 update H, full step
 update U to next time needed

 This routine does not refresh the antihermitian momenta.
 This routine begins at "integral" time, with H and U evaluated
 at same time.
 */
#include "ks_imp_includes.h"	/* definitions files and prototypes */
#include "macrologger.h"
#ifdef HAVE_QUARC
#include "quicq_milc_payload.h"
#include "quicq_milc_payload_builder.h"
#include "quicq_ks_congrad_eo_wrapper.h"
#endif

int update ()
{
  int step, iters = 0;
  Real final_rsq;
#ifdef HMC_ALGORITHM
  double startaction,endaction,d_action();
  Real xrandom;
#endif
  LOG_INFO("QUICQ-INFO : Update the momenta...");
  /* refresh the momenta */
  ranmom();

  /* do "steps" microcanonical steps"  */
  for (step = 1; step <= steps; step++) {

#ifdef PHI_ALGORITHM
    /* generate a pseudofermion configuration only at start*/
    /* also clear xxx, since zero is our best guess for the solution
     with a new random phi field. */
    if(step==1) {
      load_ferm_links(&fn_links, &ks_act_paths);
      clear_latvec( F_OFFSET(xxx1), EVENANDODD );
      grsource_imp( F_OFFSET(phi1), mass1, EVEN, &fn_links);
      clear_latvec( F_OFFSET(xxx2), EVENANDODD );
      grsource_imp( F_OFFSET(phi2), mass2, EVEN, &fn_links);
    }

#ifdef HMC_ALGORITHM
    LOG_INFO("QUICQ-INFO : HMC_ALGORITHM...");
    /* find action */
    /* do conjugate gradient to get (Madj M)inverse * phi */
    if(step==1) {
      /* do conjugate gradient to get (Madj M)inverse * phi */
      load_ferm_links(&fn_links, &ks_act_paths);
      iters += ks_congrad( F_OFFSET(phi1), F_OFFSET(xxx1), mass1,
          niter, nrestart, rsqmin, PRECISION, EVEN,
          &final_rsq, &fn_links);
      load_ferm_links(&fn_links, &ks_act_paths);
      iters += ks_congrad( F_OFFSET(phi2), F_OFFSET(xxx2), mass2,
          niter, nrestart, rsqmin, PRECISION, EVEN,
          &final_rsq, &fn_links );

      startaction=d_action();
      /* copy link field to old_link */
      gauge_field_copy_from_fm( FM_START(link[0]), F_OFFSET(old_link[0]));
    }
#endif

    /* update U's to middle of interval */
    update_u(0.5*epsilon);

#else /* "R" algorithm */
    LOG_INFO("QUICQ-INFO : R_ALGORITHM...");
    /* first update the U's to special time interval */
    /* and generate a pseudofermion configuration */
    /* probably makes most sense if nflavors1 >= nflavors2 */
    LOG_INFO("QUICQ-INFO : Update using R algorithm...");
    update_u(epsilon * (0.5 - nflavors1 / 8.0));
    clear_latvec(F_OFFSET(xxx1), EVENANDODD);
    load_ferm_links(&fn_links, &ks_act_paths);
    grsource_imp(F_OFFSET(phi1), mass1, EVEN, &fn_links);

    update_u(epsilon * ((nflavors1 - nflavors2) / 8.0));
    clear_latvec(F_OFFSET(xxx2), EVENANDODD);
    load_ferm_links(&fn_links, &ks_act_paths);
    grsource_imp(F_OFFSET(phi2), mass2, EVEN, &fn_links);

    /* update U's to middle of interval */
    update_u(epsilon * nflavors2 / 8.0);
#endif

    /* do conjugate gradient to get (Madj M)inverse * phi */
    load_ferm_links(&fn_links, &ks_act_paths);
#if 1 //HAVE_QUARC
    milc_cg_payload_t mcgp;
    quark_invert_control qic;

    // Get the blocking factors from MILC
    const int * ldims = get_logical_dimensions();
    mcgp.dist_rtfs[0] = ldims[3];
    mcgp.dist_rtfs[1] = ldims[2];
    mcgp.dist_rtfs[2] = ldims[1];
    mcgp.dist_rtfs[3] = ldims[0];

    mcgp.gs_exts[0] = nt;
    mcgp.gs_exts[1] = nz;
    mcgp.gs_exts[2] = ny;
    mcgp.gs_exts[3] = nx;
    node0_printf("Dist RTF %d %d %d %d\n",
                 mcgp.dist_rtfs[0], mcgp.dist_rtfs[1], mcgp.dist_rtfs[2],
                 mcgp.dist_rtfs[3]);

    su3_vector* src1  = get_su3_vectors_for_all_sites_on_node(F_OFFSET(phi1));
    su3_vector* dest1 = get_su3_vectors_for_all_sites_on_node(F_OFFSET(xxx1));
    su3_vector* src2  = get_su3_vectors_for_all_sites_on_node(F_OFFSET(phi2));
    su3_vector* dest2 = get_su3_vectors_for_all_sites_on_node(F_OFFSET(xxx2));
    su3_matrix* fl    = get_packed_fatlinks_for_all_sites_on_node();
    su3_matrix* ll    = get_packed_longlinks_for_all_sites_on_node();

    /* Pack structure */
    qic.prec      = PRECISION;  /* Currently ignored */
    qic.parity    = EVEN;
    qic.max       = niter;
    qic.nrestart  = nrestart;
    qic.resid     = rsqmin;
    qic.relresid  = 0;     /* Suppresses this test */

    mcgp.src1  = src1;
    mcgp.dest1 = dest1;
    mcgp.src2  = src2;
    mcgp.dest2 = dest2;
    mcgp.fat_links = fl;
    mcgp.long_links = ll;

    iters += quicq_ks_congrad_two_src(&mcgp, &qic, mass1, mass2, &final_rsq);

    /* Map solution to site structure */
    site *s;
    int i;
    FORALLSITES(i,s){
      *((su3_vector *)F_PT(s,F_OFFSET(xxx1))) = mcgp.dest1[i];
      *((su3_vector *)F_PT(s,F_OFFSET(xxx2))) = mcgp.dest2[i];
    }

    free(src1);
    free(src2);
    free(dest1);
    free(dest2);
    free(fl);
    free(ll);

#else
    LOG_INFO("QUICQ-INFO : ks_congrad_two_src...");
    node0_printf("Niter %d Nrestart %d rsqmin %f \n", niter, nrestart, rsqmin);
    iters += ks_congrad_two_src(F_OFFSET(phi1), F_OFFSET(phi2), F_OFFSET(xxx1),
                                F_OFFSET(xxx2), mass1, mass2, niter, nrestart,
                                rsqmin, PRECISION, EVEN, &final_rsq, &fn_links);

#endif
    dslash_site(F_OFFSET(xxx1), F_OFFSET(xxx1), ODD, &fn_links);
    dslash_site(F_OFFSET(xxx2), F_OFFSET(xxx2), ODD, &fn_links);
    /* now update H by full time interval */
    update_h(epsilon);

    /* update U's by half time step to get to even time */
    update_u(epsilon * 0.5);

    /* reunitarize the gauge field */
    rephase( OFF);
    reunitarize();
    rephase( ON);

  } /* end loop over microcanonical steps */

#ifdef HMC_ALGORITHM
  /* find action */
  /* do conjugate gradient to get (Madj M)inverse * phi */
  load_ferm_links(&fn_links, &ks_act_paths);
  iters += ks_congrad( F_OFFSET(phi1), F_OFFSET(xxx1), mass1,
      niter, nrestart, rsqmin, PRECISION, EVEN,
      &final_rsq, &fn_links );
  iters += ks_congrad( F_OFFSET(phi2), F_OFFSET(xxx2), mass2,
      niter, nrestart, rsqmin, PRECISION, EVEN,
      &final_rsq, &fn_links );
  endaction=d_action();
  /* decide whether to accept, if not, copy old link field back */
  /* careful - must generate only one random number for whole lattice */
  if(this_node==0)xrandom = myrand(&node_prn);
  broadcast_float(&xrandom);
  if( exp( (double)(startaction-endaction) ) < xrandom ) {
    if(steps > 0)
    gauge_field_copy_to_fm( F_OFFSET(old_link[0]), FM_START(link[0]) );
#ifdef FN
    free_fn_links(&fn_links);
    free_fn_links(&fn_links_dmdu0);
#endif
    node0_printf("REJECT: delta S = %e\n", (double)(endaction-startaction));
  }
  else {
    node0_printf("ACCEPT: delta S = %e\n", (double)(endaction-startaction));
  }
#endif

  if (steps > 0)
    return (iters / steps);
  else
    return (-99);
}

