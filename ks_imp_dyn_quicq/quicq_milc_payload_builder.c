#include "quicq_milc_payload_builder.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include "QUARC_wrapper.h"

static const int PACKED_GAUGES_PER_SITE = 8;

////////////////////////////// Private Helpers /////////////////////////////////

/// Copy of load_fatbacklinks (fermion_links_helpers.c), without the adjoint.
///
static su3_matrix* load_fatbacklinks_without_adjoint (ferm_links_t *fn)
{
  su3_matrix *t_fbl = NULL;
  su3_matrix *t_fl = fn->fat;
  su3_matrix *tempmat1 = NULL;
  msg_tag *tag[4];
  char myname[] = "load_fatbacklinks";

  // Allocate space for t_fbl if NULL
  t_fbl = (su3_matrix *)malloc(sites_on_node*4*sizeof(su3_matrix));
  if(t_fbl==NULL){
    printf("%s(%d): no room for t_fbl\n",myname,this_node);
    terminate(1);
  }
  
  tempmat1 = (su3_matrix *)malloc(sites_on_node*sizeof(su3_matrix));
  if(tempmat1 == NULL){
    printf("%s: Can't malloc temporary\n",myname);
    terminate(1);
  }
  
  // gather backwards fatlinks
  for(int dir=XUP; dir<=TUP; dir ++) {
    for (int i = 0; i < sites_on_node; i++) {
      tempmat1[i] = t_fl[dir+4*i];
    }
    tag[dir] = start_gather_field(tempmat1,
                                  sizeof(su3_matrix),
                                  OPP_DIR(dir),
                                  EVENANDODD,
                                  gen_pt[dir]);
    wait_gather( tag[dir] );
    for (int i = 0; i < sites_on_node; i++) {
      su3_matrix* temp_ = (t_fbl + dir + 4*i);
      *temp_ = *((su3_matrix *)gen_pt[dir][i]);
    }
    cleanup_gather( tag[dir] );
  }
  
  free(tempmat1); 
  tempmat1 = NULL;
  
  return t_fbl;
}

/// Copy of load_longbacklinks (fermion_links_helpers.c), without the adjoint.
///
static su3_matrix* load_longbacklinks_without_adjoint (ferm_links_t *fn)
{
  su3_matrix *t_lbl = NULL;
  su3_matrix *t_ll = fn->lng;
  su3_matrix *tempmat1 = NULL;
  msg_tag *tag[4];
  char myname[] = "load_longbacklinks";

  /// Allocate space for t_lbl if NULL
  t_lbl = (su3_matrix *)malloc(sites_on_node*4*sizeof(su3_matrix));
  if(t_lbl==NULL){
    printf("%s(%d): no room for t_lbl\n",myname,this_node);
    terminate(1);
  }
      
  tempmat1 = (su3_matrix *)malloc(sites_on_node*sizeof(su3_matrix));
  if(tempmat1 == NULL){
    printf("%s: Can't malloc temporary\n",myname);
    terminate(1);
  }
  
  /// gather backwards longlinks
  for(int dir=XUP; dir<=TUP; dir ++){
    for (int i = 0; i < sites_on_node; i++) {
      tempmat1[i] = t_ll[dir+4*i];
    }
    tag[dir] = start_gather_field(tempmat1,
                                  sizeof(su3_matrix),
                                  OPP_3_DIR(DIR3(dir)),
                                  EVENANDODD,
                                  gen_pt[dir]);
    wait_gather( tag[dir] );
    for (int i = 0; i < sites_on_node; i++) {
      su3_matrix * temp_ = (t_lbl + dir + 4*i);
      *temp_ = *((su3_matrix *)gen_pt[dir][i]);
    }
    cleanup_gather(tag[dir]);
  }
  
  free(tempmat1); 
  tempmat1 = NULL;
  
  return t_lbl;
}

//////////////////////////// End of private helpers ////////////////////////////

/// Copy su3_vectors from the lattice site major array to a field major array.
su3_vector* get_su3_vectors_for_all_sites_on_node (field_offset ft)
{
  int i;
  site *s;
  su3_vector *tmp;

  if((tmp=(su3_vector *) malloc((sites_on_node)*sizeof(su3_vector))) == NULL) {
    printf("%s\n","ERROR: No room for temporaries");
    exit(1);
  }

  FORSOMEPARITY(i,s,EVENANDODD) {
    tmp[i] = *((su3_vector *)F_PT(s,ft));
  }

  return tmp;
}

su3_matrix* get_packed_fatlinks_for_all_sites_on_node (void)
{
  int i;
  site *s;
  int num_bytes;

  num_bytes = sizeof(su3_matrix)*sites_on_node*PACKED_GAUGES_PER_SITE;
  su3_matrix* packed = (su3_matrix*)(malloc(num_bytes));
  su3_matrix* t_fatlink = fn_links.fat;
  su3_matrix* t_fatback = load_fatbacklinks_without_adjoint(&fn_links);

  FORSOMEPARITY(i,s,EVENANDODD) {
    memcpy(&packed[i*8], &t_fatlink[4*i], 4*sizeof(su3_matrix));
    memcpy(&packed[i*8+4], &t_fatback[4*i], 4*sizeof(su3_matrix));
  }

  free(t_fatback);

  return packed;
}

su3_matrix* get_packed_longlinks_for_all_sites_on_node (void)
{
  int i;
  site *s;
  int num_bytes;

  num_bytes = sizeof(su3_matrix)*sites_on_node*PACKED_GAUGES_PER_SITE;
  su3_matrix* packed = (su3_matrix*)(malloc(num_bytes));

  su3_matrix* t_longlink = fn_links.lng;
  su3_matrix* t_longback = load_longbacklinks_without_adjoint(&fn_links);

  FORSOMEPARITY(i,s,EVENANDODD) {
    memcpy(&packed[i*8], &t_longlink[4*i], 4*sizeof(su3_matrix));
    memcpy(&packed[i*8+4], &t_longback[4*i], 4*sizeof(su3_matrix));
  }

  free(t_longback);

  return packed;
}
