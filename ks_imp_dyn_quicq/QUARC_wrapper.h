#ifndef QUARC_WRAPPER_H_
#define QUARC_WRAPPER_H_

#ifdef __cplusplus
extern "C" {
#endif

void initQuarc (int *argc, char ***argv);

void finalizeQuarc ();

#ifdef __cplusplus
}
#endif

#endif /* QUARC_WRAPPER_H_ */
