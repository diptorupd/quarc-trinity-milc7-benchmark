/******** make_lattice.c *********/

/* Common to most applications */

/* 1. Allocates space for the lattice fields, as specified by the
      application site structure.  Fills in coordinates, parity, index.
   2. Allocates gen_pt pointers for gather results 
   3. Initializes site-based random number generator, if specified
      by macro SITERAND */

#include "generic_includes.h"
#include <defines.h>                 /* For SITERAND */
#include <assert.h>
#include <omp.h>

void make_lattice(){
  register int i;		/* scratch */
  int x,y,z,t;		/* coordinates */
  site *s;
  int num_fields = 0;
  int num_blocks = 0;
  int k;
  char ccc = 'c';

  fsu3_matrix mat1;
  /* allocate space for lattice, fill in parity, coordinates and index */
  num_fields = 4;  /* *link[4] */
  node0_printf("Mallocing %.1f MBytes per node for lattice\n",
	       (double)(sites_on_node * (sizeof(site) + num_fields*sizeof(*fields)))/1e6);
  lattice = (site *)malloc( sites_on_node * sizeof(site) );
  if(lattice==NULL){
    printf("NODE %d: no room for lattice\n",this_node);
    terminate(1);
  }
  fields = (su3_matrix *)malloc( num_fields * sites_on_node * sizeof(*fields) );
  if(fields==NULL){
    printf("NODE %d: no room for lattice fields\n",this_node);
    terminate(1);
  }

#define ASSIGN_FIELD(name) \
  s->name = &fields[i + sites_on_node * num_blocks]; \
  num_blocks++;

  assert(4 <= num_fields);

#pragma omp parallel for private(s)
  for (i = 0; i < sites_on_node; i++) {
    s = lattice + i;
    s->link[0] = &fields[i];
    s->link[1] = &fields[i + sites_on_node];
    s->link[2] = &fields[i + sites_on_node * 2];
    s->link[3] = &fields[i + sites_on_node * 3];
  }

  /* Allocate address vectors */
  for(i=0;i<N_POINTERS;i++){
    gen_pt[i] = (char **)malloc(sites_on_node*sizeof(char *) );
    if(gen_pt[i]==NULL){
      printf("NODE %d: no room for pointer vector\n",this_node);
      terminate(1);
    }
  }
  
  for(t=0;t<nt;t++)for(z=0;z<nz;z++)for(y=0;y<ny;y++)for(x=0;x<nx;x++){
    if(node_number(x,y,z,t)==mynode()){
      i=node_index(x,y,z,t);
      lattice[i].x=x;	lattice[i].y=y;	lattice[i].z=z;	lattice[i].t=t;
      lattice[i].index = x+nx*(y+ny*(z+nz*t));
      if( (x+y+z+t)%2 == 0)lattice[i].parity=EVEN;
      else	         lattice[i].parity=ODD;
#ifdef SITERAND
      initialize_prn( &(lattice[i].site_prn) , iseed, lattice[i].index);
#endif
    }
  }
}

void free_lattice()
{
  int i;

  for(i=0;i<N_POINTERS;i++)
    free(gen_pt[i]);

  free(lattice);
}
