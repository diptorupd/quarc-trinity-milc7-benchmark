#  Generic routines for Kogut-Susskind fermions
#  MIMD version 7
#
#  Generic make-include file for generic_ks codes
#  This template file defines rules and macros common to all applications
#  It is intended to be an include file for other Makefiles.
#  Don't use it by itself!
#
#  The paths are relative to the application directory.

G_KS_ALL = \
  d_congrad5.cppacs.o \
  d_congrad5.o \
  d_congrad5_eo.o \
  d_congrad5_fn.o \
  d_congrad5_fn.old.o \
  d_congrad5_fn_fewsums.o \
  d_congrad5_fn_qop.o \
  d_congrad5_fn_qop_D.o \
  d_congrad5_fn_qop_F.o \
  d_congrad5_fn_qop_milc_D.o \
  d_congrad5_fn_qop_milc_F.o \
  d_congrad5_fn_qop_two_src.o \
  d_congrad5_fn_qopdp.o \
  d_congrad5_fn_qopqdp_two_src.o \
  d_congrad5_fn_two_src2.o \
  d_congrad5_hl.o \
  d_congrad5_two_src.o \
  d_congrad6.o \
  d_congrad_opt.o \
  dslash.o \
  dslash_eo.o \
  dslash_fn.o \
  dslash_fn2.o \
  dslash_fn_dblstore.o \
  dslash_fn_multi.o \
  dslash_fn_qop_milc_D.o \
  dslash_fn_qop_milc_F.o \
  eigen_stuff.o \
  f_meas.o \
  fermion_force_asqtad.o \
  fermion_force_asqtad_qdp.o \
  fermion_force_asqtad_qdp_D.o \
  fermion_force_asqtad_qdp_F.o \
  fermion_force_asqtad_qop.o \
  fermion_force_asqtad_qop_D.o \
  fermion_force_asqtad_qop_F.o \
  fermion_force_asqtad_qop_milc.o \
  fermion_force_asqtad_qop_milc_D.o \
  fermion_force_asqtad_qop_milc_F.o \
  fermion_force_fn_multi.o \
  fermion_force_fn_multi_qdp.o \
  fermion_force_general.o \
  fermion_force_multi.o \
  fermion_links_asqtad_qdp.o \
  fermion_links_asqtad_qop.o \
  fermion_links_asqtad_qop_D.o \
  fermion_links_asqtad_qop_F.o \
  fermion_links_fn_qop_milc_D.o \
  fermion_links_fn_qop_milc_F.o \
  fermion_links_fn.o \
  fermion_links_fn_dmdu0.o \
  fermion_links_helpers.o \
  ff_opt.o \
  flavor_ops.o \
  fpi_2.o \
  grsource.o \
  io_helpers_ks.o \
  io_prop_ks.o \
  io_prop_ks_fm.o \
  io_scidac_ks.o \
  jacobi.o \
  ks_invert.o \
  ks_multicg.o \
  ks_multicg_offset.o \
  ks_multicg_offset_qop.o \
  ks_multicg_offset_qop_D.o \
  ks_multicg_offset_qop_F.o \
  ks_multicg_qop_milc_D.o \
  ks_multicg_qop_milc_F.o \
  load_qop_asqtad_coeffs_D.o \
  load_qop_asqtad_coeffs_F.o \
  mat_invert.o \
  mu.c \
  mu_fast.c \
  multimass_inverter.o \
  path_transport.o \
  nl_spectrum.o \
  quark_stuff.o \
  quark_stuff4.o \
  quark_stuff5.o \
  quark_stuff6.o \
  show_generic_ks_opts.o \
  smear_links.o \
  spectrum.o \
  spectrum2.o \
  spectrum_fzw.o \
  spectrum_hybrids5.o \
  spectrum_mom.o \
  spectrum_multimom.o \
  spectrum_nd.o \
  spectrum_nlpi2.o \
  spectrum_pt.o \
  spectrum_singlets.o

# Must match our headers in generic_ks_includes.h
G_KS_HEADERS = \
  ../include/config.h \
  ../include/random.h \
  lattice.h \
  ../include/complex.h \
  ../include/su3.h \
  ../include/macros.h \
  ../include/comdefs.h \
  ../include/generic.h \
  ../include/generic_quark_types.h \
  ../include/generic_ks.h \
  ../include/generic_ks_qdp.h \
  ../include/generic_ks_qop.h \
  ../include/io_scidac_ks.h

G_KS_ALL_DEPEND = \
  ../generic_ks/generic_ks_includes.h \
  ../generic_ks/Make_template \
  ../include \
  ${LASTMAKE} \
  ${G_KS_HEADERS}

${G_KS_ALL} : ${G_KS_ALL_DEPEND}

d_congrad5.cppacs.o: ../generic_ks/d_congrad5.cppacs.c
	${CC} -c ${CFLAGS} $<
d_congrad5.o: ../generic_ks/d_congrad5.c
	${CC} -c ${CFLAGS} $<
d_congrad5_hl.o: ../generic_ks/d_congrad5_hl.c
	${CC} -c ${CFLAGS} $<
d_congrad5_eo.o: ../generic_ks/d_congrad5_eo.c
	${CC} -c ${CFLAGS} $<
d_congrad5_fn.o: ../generic_ks/d_congrad5_fn.c
	${CC} -c ${CFLAGS} $<
d_congrad5_fn.old.o: ../generic_ks/d_congrad5_fn.old.c
	${CC} -c ${CFLAGS} $<
d_congrad5_fn_qop.o: ../generic_ks/d_congrad5_fn_qop.c
	${CC} -c ${CFLAGS} $<
d_congrad5_fn_qop_D.o: ../generic_ks/d_congrad5_fn_qop_D.c ../generic_ks/d_congrad5_fn_qop_P.c 
	${CC} -c ${CFLAGS} $<
d_congrad5_fn_qop_F.o: ../generic_ks/d_congrad5_fn_qop_F.c ../generic_ks/d_congrad5_fn_qop_P.c
	${CC} -c ${CFLAGS} $<
d_congrad5_fn_qop_milc_D.o: ../generic_ks/d_congrad5_fn_qop_milc_D.c ../generic_ks/d_congrad5_fn_qop_milc_P.c
	${CC} -c ${CFLAGS} $<
d_congrad5_fn_qop_milc_F.o: ../generic_ks/d_congrad5_fn_qop_milc_F.c ../generic_ks/d_congrad5_fn_qop_milc_P.c
	${CC} -c ${CFLAGS} $<
d_congrad5_fn_qop_two_src.o: ../generic_ks/d_congrad5_fn_qop_two_src.c
	${CC} -c ${CFLAGS} $<
d_congrad5_fn_qopqdp.o: ../generic_ks/d_congrad5_fn_qopqdp.c
	${CC} -c ${CFLAGS} $<
d_congrad5_fn_qopqdp_two_src.o: ../generic_ks/d_congrad5_fn_qopqdp_two_src.c
	${CC} -c ${CFLAGS} $<
d_congrad5_fn_fewsums.o: ../generic_ks/d_congrad5_fn_fewsums.c
	${CC} -c ${CFLAGS} $<
d_congrad5_two_src.o: ../generic_ks/d_congrad5_two_src.c
	${CC} -c ${CFLAGS} $<
d_congrad5_fn_two_src2.o: ../generic_ks/d_congrad5_fn_two_src2.c
	${CC} -c ${CFLAGS} $<
d_congrad6.o: ../generic_ks/d_congrad6.c
	${CC} -c ${CFLAGS} $<
d_congrad_opt.o: ../generic_ks/d_congrad_opt.c
	${CC} -c ${CFLAGS} $<
dslash.o: ../generic_ks/dslash.c
	${CC} -c ${CFLAGS} $<
dslash_eo.o: ../generic_ks/dslash_eo.c
	${CC} -c ${CFLAGS} $<
dslash_fn.o: ../generic_ks/dslash_fn.c
	${CC} -c ${CFLAGS} $<
dslash_fn2.o: ../generic_ks/dslash_fn2.c
	${CC} -c ${CFLAGS} $<
dslash_fn_dblstore.o: ../generic_ks/dslash_fn_dblstore.c
	${CC} -c ${CFLAGS} $<
dslash_fn_multi.o: ../generic_ks/dslash_fn_multi.c
	${CC} -c ${CFLAGS} $<
dslash_fn_qop_milc_D.o: ../generic_ks/dslash_fn_qop_milc_D.c ../generic_ks/dslash_fn_qop_milc_P.c
	${CC} -c ${CFLAGS} $<
dslash_fn_qop_milc_F.o: ../generic_ks/dslash_fn_qop_milc_F.c ../generic_ks/dslash_fn_qop_milc_P.c
	${CC} -c ${CFLAGS} $<
eigen_stuff.o: ../generic_ks/eigen_stuff.c
	${CC} -c ${CFLAGS} $<
fermion_force_asqtad.o: ../generic_ks/fermion_force_asqtad.c
	${CC} -c ${CFLAGS} $<
fermion_force_asqtad_qdp.o: ../generic_ks/fermion_force_asqtad_qdp.c
	${CC} -c ${CFLAGS} $<
fermion_force_asqtad_qdp_D.o: ../generic_ks/fermion_force_asqtad_qdp_D.c ../generic_ks/fermion_force_asqtad_qdp_P.c 
fermion_force_asqtad_qdp_D.o: ../generic_ks/fermion_force_asqtad_qdp_D.c
	${CC} -c ${CFLAGS} $<
fermion_force_asqtad_qdp_F.o: ../generic_ks/fermion_force_asqtad_qdp_F.c ../generic_ks/fermion_force_asqtad_qdp_P.c 
fermion_force_asqtad_qdp_F.o: ../generic_ks/fermion_force_asqtad_qdp_F.c
	${CC} -c ${CFLAGS} $<
fermion_force_asqtad_qop.o: ../generic_ks/fermion_force_asqtad_qop.c
	${CC} -c ${CFLAGS} $<
fermion_force_asqtad_qop_F.o: ../generic_ks/fermion_force_asqtad_qop_F.c ../generic_ks/fermion_force_asqtad_qop_P.c 
fermion_force_asqtad_qop_F.o: ../generic_ks/fermion_force_asqtad_qop_F.c
	${CC} -c ${CFLAGS} $<
fermion_force_asqtad_qop_D.o: ../generic_ks/fermion_force_asqtad_qop_D.c ../generic_ks/fermion_force_asqtad_qop_P.c 
fermion_force_asqtad_qop_D.o: ../generic_ks/fermion_force_asqtad_qop_D.c
	${CC} -c ${CFLAGS} $<
fermion_force_asqtad_qop_milc.o: ../generic_ks/fermion_force_asqtad_qop_milc.c
	${CC} -c ${CFLAGS} $<
fermion_force_asqtad_qop_milc_D.o: ../generic_ks/fermion_force_asqtad_qop_milc_D.c ../generic_ks/fermion_force_asqtad_qop_milc_P.c
	${CC} -c ${CFLAGS} $<
fermion_force_asqtad_qop_milc_F.o: ../generic_ks/fermion_force_asqtad_qop_milc_F.c  ../generic_ks/fermion_force_asqtad_qop_milc_P.c
	${CC} -c ${CFLAGS} $<
fermion_force_fn_multi.o: ../generic_ks/fermion_force_fn_multi.c
	${CC} -c ${CFLAGS} $<
fermion_force_fn_multi_qdp.o: ../generic_ks/fermion_force_fn_multi_qdp.c
	${CC} -c ${CFLAGS} $<
fermion_force_general.o: ../generic_ks/fermion_force_general.c
	${CC} -c ${CFLAGS} $<
fermion_force_multi.o: ../generic_ks/fermion_force_multi.c
	${CC} -c ${CFLAGS} $<
f_meas.o: ../generic_ks/f_meas.c
	${CC} -c ${CFLAGS} $<
fpi_2.o: ../generic_ks/fpi_2.c
	${CC} -c ${CFLAGS} $<
flavor_ops.o: ../generic_ks/flavor_ops.c
	${CC} -c ${CFLAGS} $<
fermion_links_asqtad_qdp.o: ../generic_ks/fermion_links_asqtad_qdp.c
	${CC} -c ${CFLAGS} $<
fermion_links_asqtad_qop.o: ../generic_ks/fermion_links_asqtad_qop.c
	${CC} -c ${CFLAGS} $<
fermion_links_asqtad_qop_D.o: ../generic_ks/fermion_links_asqtad_qop_D.c  ../generic_ks/fermion_links_asqtad_qop_P.c
	${CC} -c ${CFLAGS} $<
fermion_links_asqtad_qop_F.o: ../generic_ks/fermion_links_asqtad_qop_F.c  ../generic_ks/fermion_links_asqtad_qop_P.c
	${CC} -c ${CFLAGS} $<
fermion_links_fn_qop_milc_D.o: ../generic_ks/fermion_links_fn_qop_milc_D.c ../generic_ks/fermion_links_fn_qop_milc_P.c
	${CC} -c ${CFLAGS} $<
fermion_links_fn_qop_milc_F.o: ../generic_ks/fermion_links_fn_qop_milc_F.c ../generic_ks/fermion_links_fn_qop_milc_P.c
	${CC} -c ${CFLAGS} $<
fermion_links_fn.o: ../generic_ks/fermion_links_fn.c
	${CC} -c ${CFLAGS} $<
fermion_links_fn_dmdu0.o: ../generic_ks/fermion_links_fn_dmdu0.c
	${CC} -c ${CFLAGS} $<
fermion_links_helpers.o: ../generic_ks/fermion_links_helpers.c
	${CC} -c ${CFLAGS} $<
ff_opt.o: ../generic_ks/ff_opt.c
	${CC} -c ${CFLAGS} $<
grsource.o: ../generic_ks/grsource.c
	${CC} -c ${CFLAGS} $<
grsource_imp.o: ../generic_ks/grsource_imp.c
	${CC} -c ${CFLAGS} $<
io_helpers_ks.o: ../generic_ks/io_helpers_ks.c
	${CC} -c ${CFLAGS} $<
io_prop_ks.o: ../generic_ks/io_prop_ks.c
	${CC} -c ${CFLAGS} $<
io_prop_ks_fm.o: ../generic_ks/io_prop_ks_fm.c
	${CC} -c ${CFLAGS} $<
io_scidac_ks.o: ../generic_ks/io_scidac_ks.c
	${CC} -c ${CFLAGS} $<
jacobi.o: ../generic_ks/jacobi.c
	${CC} -c ${CFLAGS} $<
ks_invert.o: ../generic_ks/ks_invert.c
	${CC} -c ${CFLAGS} $<
ks_multicg.o: ../generic_ks/ks_multicg.c
	${CC} -c ${CFLAGS} $<
ks_multicg_offset.o: ../generic_ks/ks_multicg_offset.c
	${CC} -c ${CFLAGS} $<
ks_multicg_offset_qop.o: ../generic_ks/ks_multicg_offset_qop.c
	${CC} -c ${CFLAGS} $<
ks_multicg_offset_qop_D.o: ../generic_ks/ks_multicg_offset_qop_D.c  ../generic_ks/ks_multicg_offset_qop_P.c
	${CC} -c ${CFLAGS} $<
ks_multicg_offset_qop_F.o: ../generic_ks/ks_multicg_offset_qop_F.c ../generic_ks/ks_multicg_offset_qop_P.c
	${CC} -c ${CFLAGS} $<
ks_multicg_qop_milc_D.o: ../generic_ks/ks_multicg_qop_milc_D.c ../generic_ks/ks_multicg_qop_milc_P.c
	${CC} -c ${CFLAGS} $<
ks_multicg_qop_milc_F.o: ../generic_ks/ks_multicg_qop_milc_F.c ../generic_ks/ks_multicg_qop_milc_P.c
	${CC} -c ${CFLAGS} $<
load_qop_asqtad_coeffs_D.o: ../generic_ks/load_qop_asqtad_coeffs_D.c ../generic_ks/load_qop_asqtad_coeffs_P.c
	${CC} -c ${CFLAGS} $<
load_qop_asqtad_coeffs_F.o: ../generic_ks/load_qop_asqtad_coeffs_F.c ../generic_ks/load_qop_asqtad_coeffs_P.c
	${CC} -c ${CFLAGS} $<
mat_invert.o: ../generic_ks/mat_invert.c
	${CC} -c ${CFLAGS} $<
mu.o: ../generic_ks/mu.c
	${CC} -c ${CFLAGS} $<
mu_fast.o: ../generic_ks/mu_fast.c
	${CC} -c ${CFLAGS} $<
multimass_inverter.o: ../generic_ks/multimass_inverter.c
	${CC} -c ${CFLAGS} $<
nl_spectrum.o: ../generic_ks/nl_spectrum.c
	${CC} -c ${CFLAGS} $<
path_transport.o: ../generic_ks/path_transport.c
	${CC} -c ${CFLAGS} $<
quark_stuff.o: ../generic_ks/quark_stuff.c
	${CC} -c ${CFLAGS} $<
quark_stuff4.o: ../generic_ks/quark_stuff4.c
	${CC} -c ${CFLAGS} $<
quark_stuff5.o: ../generic_ks/quark_stuff5.c
	${CC} -c ${CFLAGS} $<
quark_stuff6.o: ../generic_ks/quark_stuff6.c
	${CC} -c ${CFLAGS} $<
rephase.o: ../generic_ks/rephase.c
	${CC} -c ${CFLAGS} $<
show_generic_ks_opts.o: ../generic_ks/show_generic_ks_opts.c
	${CC} -c ${CFLAGS} $<
smear_links.o: ../generic_ks/smear_links.c
	${CC} -c ${CFLAGS} $<
spectrum.o: ../generic_ks/spectrum.c
	${CC} -c ${CFLAGS} $<
spectrum_hybrids5.o: ../generic_ks/spectrum_hybrids5.c
	${CC} -c ${CFLAGS} $<
spectrum2.o: ../generic_ks/spectrum2.c
	${CC} -c ${CFLAGS} $<
spectrum_fzw.o: ../generic_ks/spectrum_fzw.c
	${CC} -c ${CFLAGS} $<
spectrum_mom.o: ../generic_ks/spectrum_mom.c
	${CC} -c ${CFLAGS} $<
spectrum_multimom.o: ../generic_ks/spectrum_multimom.c
	${CC} -c ${CFLAGS} $<
spectrum_nd.o: ../generic_ks/spectrum_nd.c
	${CC} -c ${CFLAGS} $<
spectrum_pt.o: ../generic_ks/spectrum_pt.c
	${CC} -c ${CFLAGS} $<
spectrum_nlpi2.o: ../generic_ks/spectrum_nlpi2.c
	${CC} -c ${CFLAGS} $<
spectrum_singlets.o: ../generic_ks/spectrum_singlets.c
	${CC} -c ${CFLAGS} $<

ifeq ($(strip ${HAVEQDP}),true)

G_KS_ALL_QDP = \
  d_congrad5_fn_1sum_qdp.o \
  d_congrad5_fn_1sum_qdp_D.o \
  d_congrad5_fn_1sum_qdp_F.o \
  d_congrad5_fn_qdp.o \
  d_congrad5_fn_qdp_D.o \
  d_congrad5_fn_qdp_F.o \
  dslash_fn_qdp_D.o \
  dslash_fn_qdp_F.o \
  ks_multicg_offset_qdp.o \
  ks_multicg_offset_qdp_D.o \
  ks_multicg_offset_qdp_F.o

G_KS_HEADERS_QDP = \
  $(QDP)/include/qdp.h

G_KS_ALL_DEPEND_QDP = \
  ${G_KS_HEADERS_QDP} \
  ${G_KS_ALL_DEPEND}

${G_KS_ALL_QDP} : ${G_KS_ALL_DEPEND_QDP}

d_congrad5_fn_1sum_qdp.o: ../generic_ks/d_congrad5_fn_1sum_qdp.c
	${CC} -c ${CFLAGS} $<
d_congrad5_fn_1sum_qdp_D.o: ../generic_ks/d_congrad5_fn_1sum_qdp_D.c ../generic_ks/d_congrad5_fn_1sum_qdp_P.c
	${CC} -c ${CFLAGS} $<
d_congrad5_fn_1sum_qdp_F.o: ../generic_ks/d_congrad5_fn_1sum_qdp_F.c ../generic_ks/d_congrad5_fn_1sum_qdp_P.c
	${CC} -c ${CFLAGS} $<
d_congrad5_fn_qdp.o: ../generic_ks/d_congrad5_fn_qdp.c
	${CC} -c ${CFLAGS} $<
d_congrad5_fn_qdp_D.o: ../generic_ks/d_congrad5_fn_qdp_D.c ../generic_ks/d_congrad5_fn_qdp_P.c
	${CC} -c ${CFLAGS} $<
d_congrad5_fn_qdp_F.o: ../generic_ks/d_congrad5_fn_qdp_F.c ../generic_ks/d_congrad5_fn_qdp_P.c
	${CC} -c ${CFLAGS} $<
dslash_fn_qdp_D.o: ../generic_ks/dslash_fn_qdp_D.c  ../generic_ks/dslash_fn_qdp_P.c
	${CC} -c ${CFLAGS} $<
dslash_fn_qdp_F.o: ../generic_ks/dslash_fn_qdp_F.c  ../generic_ks/dslash_fn_qdp_P.c
	${CC} -c ${CFLAGS} $<
ks_multicg_offset_qdp.o: ../generic_ks/ks_multicg_offset_qdp.c
	${CC} -c ${CFLAGS} $<
ks_multicg_offset_qdp_D.o: ../generic_ks/ks_multicg_offset_qdp_D.c ../generic_ks/ks_multicg_offset_qdp_P.c
	${CC} -c ${CFLAGS} $<
ks_multicg_offset_qdp_F.o: ../generic_ks/ks_multicg_offset_qdp_F.c ../generic_ks/ks_multicg_offset_qdp_P.c
	${CC} -c ${CFLAGS} $<

endif
