#include "compare_fp_arrays.h"
#include <iomanip>
#include <iostream>
#include <stdlib.h>
#include <boost/math/special_functions/relative_difference.hpp>

#if PRECISION == 1
#define EPSILON 1e-3
#else
#define EPSILON 1e-9
#endif

using boost::math::relative_difference;

bool compare_floats (Real fp1, Real fp2)
{
#if 0
    std::cout << " A : " << fp1 << " E " << fp2 << '\n';
#endif
  auto rel_diff = relative_difference<Real,Real>(fp1, fp2);
  if(rel_diff > EPSILON) {
   std::cout << '\t'
             << std::setw(30) << std::left
             << __FUNCTION__      << " : "
             << "FAIL "
             << "( Actual " << fp1 << " "
             << "Expected "   << fp2
             << " Relative Diff : " << rel_diff
             << ")\n";
   return false;
  }
  else {
   return true;
  }
}
