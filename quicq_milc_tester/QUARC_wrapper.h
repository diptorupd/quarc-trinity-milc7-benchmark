#ifndef QUARC_WRAPPER_H_
#define QUARC_WRAPPER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "quicq_milc_payload.h"

void quicq_ks_dslash_eo_wrapper (milc_payload_t *milc_payload);

void quicq_ks_dslash_oe_wrapper (milc_payload_t *milc_payload);

Real quicq_magsq_su3vec_wrapper_eo (const milc_payload_t *milc_payload);

Real quicq_magsq_su3vec_wrapper_oe (const milc_payload_t *milc_payload);

void quicq_axpy_wrapper_e (milc_payload_t *milc_payload, Real A);

void quicq_axpy_wrapper_o (milc_payload_t *milc_payload, Real A);

Real quicq_dot_product_wrapper_e (const milc_payload_t *milc_payload);

Real quicq_dot_product_wrapper_o (const milc_payload_t *milc_payload);

#if 0
void quicq_add_su3_vector_wrapper (milc_payload_t *milc_payload);

Real quicq_norm_wrapper (const milc_payload_t *milc_payload);
#endif

#ifdef __cplusplus
}
#endif

#endif /* QUARC_WRAPPER_H_ */
