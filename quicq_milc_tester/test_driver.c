#define CONTROL
#include "quicq_milc_payload_builder.h"
#include "test_cases.h"
#include "QUARC_wrapper.h"
#include "quicq_ks_congrad_eo_wrapper.h"
#include "ks_imp_includes.h"    /* definitions files and prototypes */
#include <assert.h>
#include <stdio.h>

EXTERN gauge_header start_lat_hdr;    /* Input gauge field header */

#if PRECISION == 1
#define fptype float
#else
#define fptype double
#endif

#define GEN_DATA_FILE               0

#define TEST_DATA_LAYOUT_CONVERSION 0
#define TEST_SU3_OPS                0
#define TEST_DSLASH_FN_FIELD        1
#define TEST_BLAS                   0
#define TEST_CG                     0

static const int START    = 1;

// Print out the su3_vectors
void dump_su3_vectors (const su3_vector *src, int parity)
{
  int i;
  site *s;

  FORSOMEPARITY(i,s,parity) {
    printf(
      "Site %d (%d %d %d %d) : {{%e,%e},{%e,%e},{%e,%e}}\n",
      i, s->t, s->z , s->y, s->x,
      src[i].c[0].real,
      src[i].c[0].imag,
      src[i].c[1].real,
      src[i].c[1].imag,
      src[i].c[2].real,
      src[i].c[2].imag
    );
  }
}

//////////////////////////// test_milc_v_quicq /////////////////////////////////

void test_milc_vs_quicq (milc_payload_t *me, int parity)
{
  int output_parity;
  msg_tag *tags[16];

  if (parity == EVEN)
    output_parity = ODD;
  else if (parity == ODD)
    output_parity = EVEN;
  else {
    fprintf(stderr, "Parity can only be EVEN or ODD.\n");
    exit(1);
  }
  ////////////////////////////////////////////////////////////////////////////
  //////////////////////// Test data layout conversions //////////////////////
  ////////////////////////////////////////////////////////////////////////////

#if TEST_DATA_LAYOUT_CONVERSION
  copy_data_out_quicq_ks_spinor_latt(cb_latt_ptr, me->temp, parity);
  printf("%-75s : %s \n", "Test KS-Spinor in/out",
         compare_su3vector_arrays(me->temp, me->src1, parity) ? "PASS" :"FAIL");

  copy_data_out_quicq_fatlinks(cb_latt_ptr, me->temp_links, output_parity);
  printf("%-75s : %s \n", "Test Fat Links in/out",
         compare_links_arrays(me->temp_links,
                              me->fat_links,
                              output_parity,
                              FATLINK) ? "PASS" : "FAIL");

  copy_data_out_quicq_longlinks(cb_latt_ptr, me->temp_links, output_parity);
  printf("%-75s : %s \n", "Test Long Links in/out",
      compare_links_arrays(me->temp_links,
                           me->long_links,
                           output_parity,
                           LONGLINK) ? "PASS" : "FAIL");
#endif

#if TEST_SU3_OPS
  int i;
  site *s;
#if 0
  printf("///////// MILC OUT /////////\n");
  dump_su3_vectors(me->dest1, parity);
  printf("///////////// QUICQ OUT ///////////////\n");
  dump_su3_vectors(me->temp, parity);
#endif

    FORSOMEPARITY(i,s,parity) {
      mult_su3_mat_vec(&me->fat_links[i*4], &me->src1[i], &me->dest1[i]);
      call_quicq_mult_su3_mat_vec(&me->fat_links[i*4], &me->src1[i],
                                  &me->temp[i]);
    }
    
    printf("%-75s : %s \n", "Test mult_su3_mat_vec", 
           compare_su3_vectors(me->dest1, me->temp, parity) ? "PASS" : "FAIL");

    FORSOMEPARITY(i,s,parity) {
        mult_adj_su3_mat_vec(&me->long_links[i*4], &me->src1[i], &me->dest1[i]);
        call_quicq_mult_adj_su3_mat_vec(&me->long_links[i*4], &me->src1[i], 
                                        &me->temp[i]);
    }
    
    printf("%-75s : %s \n", "Test mult_adj_su3_mat_vec", 
           compare_su3_vectors(me->dest1, me->temp, parity) ? "PASS" : "FAIL");
    
    FORSOMEPARITY(i,s,parity) {
        for(int x=0; x<3; x++) {
            CADD( me->src2[i].c[x], me->src1[i].c[x], me->dest1[i].c[x] );
        }
        call_quicq_cadd(&me->src2[i], &me->src1[i], &me->temp[i]);
    }
    
    printf("%-75s : %s \n", "Test CADD", 
           compare_su3_vectors(me->dest1, me->temp, parity) ? "PASS" : "FAIL");

#if 0
    printf("///////// MILC OUT /////////\n");
    dump_su3_vectors(e->src2, parity);
    printf("///////////// QUICQ OUT ///////////////\n");
    dump_su3_vectors(e->src1, parity);
#endif

    FORSOMEPARITY(i,s,parity) {
        for(int x=0; x<3; x++) {
            CSUB(me->src2[i].c[x], me->src1[i].c[x], me->dest1[i].c[x] );
        }
        call_quicq_csub(&me->src2[i], &me->src1[i], &me->temp[i]);
    }
    
    printf("%-75s : %s \n", "Test CSUB", 
           compare_su3_vectors(me->dest1, me->temp, parity) ? "PASS" : "FAIL");
    
    // Clean up the destination su3_vecs, in prep for other tests
    reset_dest_vecs(me->dest1);
    reset_dest_vecs(me->temp);
#endif
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////// Compare the two dslash operators ////////////////////
    ////////////////////////////////////////////////////////////////////////////
#if TEST_DSLASH_FN_FIELD

#if 0
  /// XXXXX Calling copy_data_out_quicq_ks_spinor_latt twice would lead to bad
  /// things happening. The first call would transform the arrays, and then the
  /// second copy_data_out call would never get the right data.
  if(mynode() == 0) {
    copy_data_out_quicq_ks_spinor_latt(cb_latt_ptr, me->temp, parity);
    printf("///////////// QUARC SRC ///////////////\n");
    dump_su3_vectors(me->src1, parity);
    printf("///////////// MILC SRC ///////////////\n");
    dump_su3_vectors(me->temp, parity);
    printf("////////////////////////////\n");
  }
#endif

  /* QUICQ's ks-dslash version */
  if(output_parity == ODD) {
    quicq_ks_dslash_eo_wrapper(me);
  }
  else {
    quicq_ks_dslash_oe_wrapper(me);
  }
  double dtime = -dclock();
  for(int i = 0; i < 500; ++i) {
  /* call MILC's dslash_fn_field_special */
  dslash_fn_field_special(me->src1, me->temp, output_parity, tags, START,
                          &fn_links);
  }
  dtime +=dclock();

#if 0
  if(mynode() == 0) {
    printf("MILC Dslash time 500 iters : %e\n", dtime);
    //printf("///////// QUARC OUT /////////\n");
    //dump_su3_vectors(me->temp, output_parity);
    //printf("///////// MILC OUT /////////\n");
    //dump_su3_vectors(me->dest1, output_parity);
    //printf("////////////////////////////\n");
  }
#endif
  printf("%-75s : %s \n", "Test KS-Dslash ",
         compare_su3vector_arrays(me->temp, me->dest1, output_parity) ?
           "PASS":"FAIL");
#endif // TEST_DSLASH_FN_FIELD
  ////////////////////////////////////////////////////////////////////////////
  ///////////////////////// Compare BLAS kernels /////////////////////////////
  ////////////////////////////////////////////////////////////////////////////
#if TEST_BLAS

  //------------ Test magsq_su3vec
  double source_norm = 0.0, q_source_norm = 0.0;
  int i;
  site *s;
  FORSOMEPARITY(i, s, parity) {
    source_norm += (double)magsq_su3vec(&me->src1[i]);
  }
  g_doublesum( &source_norm );

  if(parity == EVEN) {
    q_source_norm = (double)quicq_magsq_su3vec_wrapper_eo(me);
  }
  else {
    q_source_norm = (double)quicq_magsq_su3vec_wrapper_oe(me);
  }

  printf("%-75s : %s \n", "Test magsq_su3vec ",
         compare_reals(source_norm, q_source_norm) ? "PASS":"FAIL");

  //----------- Test Axpy
  Real msq_x4 = 4.0*mass1*mass1;
  double rsq = 0;
  FORSOMEPARITYDOMAIN(i,s,parity) {
    scalar_mult_add_su3_vector(&me->src1[i],&me->src2[i],-msq_x4,&me->temp[i]);
  }

  if(parity == EVEN) {
    quicq_axpy_wrapper_e(me, -msq_x4);
  }
  else {
    quicq_axpy_wrapper_o(me, -msq_x4);
  }

  printf("%-75s : %s \n", "Test axpy ",
         compare_su3vector_arrays(me->temp, me->dest1, parity) ?
           "PASS":"FAIL");

  //------------- Test su3_rot
  double pkp = 0.0, q_pkp =0.0;

  FORSOMEPARITYDOMAIN(i,s,parity) {
    pkp += (double)su3_rdot(&me->src1[i], &me->src2[i]);
  }
  g_doublesum(&pkp);

  if(parity == EVEN) {
    double tmp = 0.0;
    for(int ii = 0; ii < even_sites_on_node; ++ii) {
      for(int j = 0; j < 3; ++j) {
        tmp += me->src1[ii].c[j].real*me->src2[ii].c[j].real;
        tmp += me->src1[ii].c[j].imag*me->src2[ii].c[j].imag;
      }
    }
    printf("TMP dot %f\n", tmp);

    q_pkp = (double)quicq_dot_product_wrapper_e(me);
  }
  else {
    q_pkp = (double)quicq_dot_product_wrapper_o(me);
  }

  printf("%-75s : %s \n", "Test dot product ",
         compare_reals(q_pkp, pkp) ? "PASS":"FAIL");

#endif
  ////////////////////////////////////////////////////////////////////////////
  ///////////////////////// Compare CG implementations ///////////////////////
  ////////////////////////////////////////////////////////////////////////////
#if TEST_CG

  Real final_rsq;
  //clear_latvec(F_OFFSET(xxx1), EVENANDODD);
  //node0_printf("///////// MILC OUT Before /////////\n");
  //dump_su3_vectors(me->src1, parity);

  milc_cg_payload_t *mcgp = create_milc_cg_payload();

  ks_congrad(F_OFFSET(phi1), F_OFFSET(xxx1), mass1, 100, 5, 0.015625,
             PRECISION, parity, &final_rsq, &fn_links);
  //me->dest1 = get_su3_vectors_for_all_sites_on_node(F_OFFSET(xxx1));
  node0_printf("Final RSQ: %f\n", final_rsq);
  //dump_su3_vectors(me->dest1, parity);

  quark_invert_control qic;
  /* Pack structure */
  qic.prec      = PRECISION;  /* Currently ignored */
  qic.parity    = parity;
  qic.max       = 100;
  qic.nrestart  = 5;
  qic.resid     = 0.015625;
  qic.relresid  = 0;     /* Suppresses this test */

  quicq_ks_cg_parity_eo_wrapper(mcgp, &qic, mass1);

  node0_printf("QUICQ Final RSQ: %f\n", qic.final_rsq);
  destroy_milc_cg_payload(mcgp);

#endif // TEST_CG
}

#if GEN_DATA_FILE
/// Dump out the MILC input and ks_dslash output to text files.
///
void generate_milc_ksd_data (const milc_payload_t *me, int parity)
{
  char su3vec_in[37];
  char fat_links_in[39];
  char long_links_in[40];
  char su3vec_out[38];
    
  FILE *fsu3vec_in;
  FILE *fsu3vec_out;
  FILE *ffat_links_in, *flong_links_in;
  int output_parity;
  msg_tag *tags[16];
  int num_sites;
  int i, src_st_ind = 0, tmp_st_ind, src_end_ind;
  site *s;
  fptype *fl, *ll;
    
#if PRECISION == 1
  if(parity == EVEN) {
    output_parity = ODD;
    sprintf(su3vec_in, "%03d_%03d_%03d_%03d_SP_E2O_su3vec.in.data",
            nt,nz,ny,nx/2);
    sprintf(fat_links_in, "%03d_%03d_%03d_%03d_SP_E2O_fatlinks.in.data",
            nt,nz,ny,nx/2);
    sprintf(long_links_in, "%03d_%03d_%03d_%03d_SP_E2O_longlinks.in.data",
            nt,nz,ny,nx/2);
    sprintf(su3vec_out, "%03d_%03d_%03d_%03d_SP_E2O_su3vec.out.data",
            nt,nz,ny,nx/2);
  }
  else if (parity == ODD) {
    output_parity = EVEN;
    sprintf(su3vec_in, "%03d_%03d_%03d_%03d_SP_O2E_su3vec.in.data", 
            nt,nz,ny,nx/2);
    sprintf(fat_links_in, "%03d_%03d_%03d_%03d_SP_O2E_fatlinks.in.data",
            nt,nz,ny,nx/2);
    sprintf(long_links_in, "%03d_%03d_%03d_%03d_SP_O2E_longlinks.in.data",
            nt,nz,ny,nx/2);
    sprintf(su3vec_out, "%03d_%03d_%03d_%03d_SP_O2E_su3vec.out.data",
            nt,nz,ny,nx/2);
  }
#else
  if(parity == EVEN) {
    output_parity = ODD;
    sprintf(su3vec_in, "%03d_%03d_%03d_%03d_DP_E2O_su3vec.in.data",
            nt,nz,ny,nx/2);
    sprintf(fat_links_in, "%03d_%03d_%03d_%03d_DP_E2O_fatlinks.in.data",
            nt,nz,ny,nx/2);
    sprintf(long_links_in, "%03d_%03d_%03d_%03d_DP_E2O_longlinks.in.data",
            nt,nz,ny,nx/2);
    sprintf(su3vec_out, "%03d_%03d_%03d_%03d_DP_E2O_su3vec.out.data",
            nt,nz,ny,nx/2);
  }
  else if (parity == ODD) {
    output_parity = EVEN;
    sprintf(su3vec_in, "%03d_%03d_%03d_%03d_DP_O2E_su3vec.in.data",
            nt,nz,ny,nx/2);
    sprintf(fat_links_in, "%03d_%03d_%03d_%03d_DP_O2E_fatlinks.in.data",
            nt,nz,ny,nx/2);
    sprintf(long_links_in, "%03d_%03d_%03d_%03d_DP_O2E_longlinks.in.data",
            nt,nz,ny,nx/2);
    sprintf(su3vec_out, "%03d_%03d_%03d_%03d_DP_O2E_su3vec.out.data",
            nt,nz,ny,nx/2);
  }
#endif
  else {
    fprintf(stderr, "Parity can only be EVEN or ODD.\n");
    exit(1);
  }
    
  fsu3vec_in = fopen(su3vec_in, "w");
  if (fsu3vec_in == NULL)     { printf("Error opening file!\n"); exit(1); }
    
  ffat_links_in = fopen(fat_links_in, "w");
  if (ffat_links_in == NULL)  { printf("Error opening file!\n"); exit(1); }
    
  flong_links_in = fopen(long_links_in, "w");
  if (flong_links_in == NULL) { printf("Error opening file!\n"); exit(1); }
    
  fsu3vec_out = fopen(su3vec_out, "w");
  if (fsu3vec_out == NULL)    { printf("Error opening file!\n"); exit(1); }

  fl = (fptype*)me->fat_links;
  ll = (fptype*)me->long_links;
    
  if (parity == EVEN) {
    output_parity = ODD;
    src_st_ind  = even_sites_on_node*8*18;
    num_sites   = odd_sites_on_node;
    src_end_ind = sites_on_node;
  }
  else if (parity == ODD) {
    output_parity = EVEN;
    src_st_ind  = 0;
    num_sites   = even_sites_on_node;
    src_end_ind = num_sites;
  }
  else {
    fprintf(stderr, "Parity can only be EVEN or ODD.\n");
    exit(1);
  }

  // Dump the su3_vector inputs from MILC
  FORSOMEPARITY(i,s,parity) {
    fprintf(fsu3vec_in,
            "%f|%f|%f|%f|%f|%f\n",
            me->src1[i].c[0].real,
            me->src1[i].c[0].imag,
            me->src1[i].c[1].real,
            me->src1[i].c[1].imag,
            me->src1[i].c[2].real,
            me->src1[i].c[2].imag);
  }

  // Dump packed input fat links
  tmp_st_ind = src_st_ind;
  for(;tmp_st_ind < src_end_ind *144; tmp_st_ind +=144) {
    for(int line = 0ul; line < 143; ++line) {
      fprintf(ffat_links_in, "%f|",fl[tmp_st_ind+line]);
    }
    fprintf(ffat_links_in, "%f\n",fl[tmp_st_ind+143]);
  }
    
  // Dump packed input long links
  tmp_st_ind = src_st_ind;
  for(;tmp_st_ind < src_end_ind *144; tmp_st_ind +=144) {
    for(int line = 0ul; line < 143; ++line) {
      fprintf(flong_links_in, "%f|",ll[tmp_st_ind+line]);
    }
    fprintf(flong_links_in, "%f\n",ll[tmp_st_ind+143]);
  }

  // Run MILC's ks-dslash
  dslash_fn_field_special(me->src1, me->dest1, output_parity,
                          tags, START, &fn_links);

  // Dump the output from MILC
  FORSOMEPARITY(i,s,output_parity) {
    fprintf(
            fsu3vec_out, 
            "%f|%f|%f|%f|%f|%f\n",
            me->dest1[i].c[0].real,
            me->dest1[i].c[0].imag,
            me->dest1[i].c[1].real,
            me->dest1[i].c[1].imag,
            me->dest1[i].c[2].real,
            me->dest1[i].c[2].imag
    );
  }

  // close the files
  fclose(fsu3vec_in);
  fclose(fsu3vec_out);
  fclose(ffat_links_in);
  fclose(flong_links_in);
}
#endif

int main( int argc, char **argv )
{
  int prompt;
  // MILC environment wrapper
  milc_payload_t *me;
  initialize_machine(&argc,&argv);
  // Remap standard I/O
  if(remap_stdio_from_args(argc, argv) == 1) terminate(1);
  g_sync();
  // set up
  prompt = setup();
  // loop over input sets
  while(readin(prompt) == 0);
  
  // Setup the MILC environment
  me = create_milc_env();
  
  node0_printf("===================== EVEN_2_ODD ==========================\n");
  test_milc_vs_quicq(me, EVEN); // Read EVEN and write to ODD
#if GEN_DATA_FILE
  generate_milc_ksd_data(me, EVEN);
#endif
#if 0
  node0_printf("===================== ODD_2_EVEN ==========================\n");
  test_milc_vs_quicq(me, ODD); // Read ODD and write to EVEN
#if GEN_DATA_FILE
  generate_milc_ksd_data(me, ODD);
#endif
#endif

  destroy_milc_env(me);
  normal_exit(0);

  return 0;
}
