#ifndef QUICQ_MILC_PAYLOAD_H_
#define QUICQ_MILC_PAYLOAD_H_

#include "../include/config.h"
#include "../include/generic_ks.h"
#include "../include/generic.h"
#include "../include/su3.h"
#include "../include/dirs.h"
#include "../ks_imp_dyn/lattice.h"

typedef struct milc_payload
{
  int gs_exts[4];
  int dist_rtfs[4];
  su3_vector *src1;
  su3_vector *src2;
  su3_vector *dest1;
  su3_vector *temp;
  su3_matrix *fat_links;
  su3_matrix *long_links;
  su3_matrix *temp_links;
} milc_payload_t;

typedef struct milc_cg_payload
{
  int gs_exts[4];
  int dist_rtfs[4];
  su3_vector *src1;
  su3_vector *dest1;
  su3_vector *src2;
  su3_vector *dest2;
  su3_matrix *fat_links;
  su3_matrix *long_links;
} milc_cg_payload_t;

#endif /* QUICQ_MILC_PAYLOAD_H_ */
