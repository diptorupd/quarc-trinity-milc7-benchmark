#include "QUARC_wrapper.h"
#include "quarc.hpp"
#include "METL/quarc_default_operators.hpp"
#include "quicq_ks_types.hpp"
#include "quicq_ks_operators.hpp"
#include "quicq_layout_transformers.hpp"
#include <iostream>
#include <err.h>

using namespace quarc_rt;
using namespace quarc::metl;
using namespace quicq;

#define HRTIMER_MAX INT64_MAX
#define HRTIMER_MIN INT64_MIN
/* resolution of the hrtimer (nanoseconds) */
#define HRTICK 1000000000

namespace
{
constexpr size_t NITERS = 500;
using hrtimer_t = int64_t;
hrtimer_t hrtimer_get()
{
#ifdef CLOCK_REALTIME
  struct timespec now;
  hrtimer_t t;

  if(clock_gettime(CLOCK_REALTIME, &now) != 0) {
    warn("clock_gettime failed");
    return -1;
  }

  t = now.tv_sec;
  t *= 1000000000L;
  t += now.tv_nsec;
  return t;
#else
  struct timeval now;
  hrtimer_t t;

  if(gettimeofday(&now, NULL) == -1) {
    warn("gettimeofday failed");
    return -1;
  }

  t = now.tv_sec;
  t *= 1000000000L;
  t += now.tv_usec * 1000L;
  return t;
#endif
}

void setKS (ks_spinor_latt &trgt, const su3_vector *src)
{
  // Fence synchronization on the window
  MPI_Win_fence(MPI_MODE_NOSTORE, trgt.get_mpi_win());
  // get the underlying raw memory
  auto raw = (QUICQ_TYPE*)trgt.get_raw_arr();
  // pointer to the milc memory allocation
  auto *milc_raw_arr  = (const QUICQ_TYPE*)src;
  // Number of floating points in local mddarray blocks
  auto total_fps = trgt.get_data_placement()->get_n_local_arr_elems();
  // Copy data
  std::memcpy(raw, milc_raw_arr, total_fps*sizeof(QUICQ_TYPE));

  // data layout transformation to go here
  if(trgt.get_data_placement()->get_simd_dim_ext() > 1) {
    quicq::transform_to_rho_phi_layout_su3ate(trgt);
  }
  MPI_Win_fence(0, trgt.get_mpi_win());
}

void getKS (ks_spinor_latt &src, su3_vector *trgt)
{
  // pointer into the milc su3_vector array
  auto *milc_raw_arr  = (QUICQ_TYPE*)trgt;
  const QUICQ_TYPE* raw;
  MPI_Win win;
  size_t total_fps;

  win = src.get_mpi_win();
  raw = (const QUICQ_TYPE*)src.get_raw_arr();
  total_fps = src.get_data_placement()->get_n_local_arr_elems();
  // Fence synchronization on the window
  MPI_Win_fence(MPI_MODE_NOSTORE, win);
  // data layout transformation back to lexicographic
  if(src.get_data_placement()->get_simd_dim_ext() > 1) {
    quicq::transform_to_lexicographic_layout_su3ate(src);
  }
  // Copy data
  std::memcpy(milc_raw_arr, raw, total_fps*sizeof(QUICQ_TYPE));
  MPI_Win_fence(0, src.get_mpi_win());
}

void setGauge (links_latt &trgt, const su3_matrix *src)
{
  MPI_Win win;
  QUICQ_TYPE *raw;
  const QUICQ_TYPE *milc_raw_arr;
  std::size_t total_fps;

  milc_raw_arr  = (const QUICQ_TYPE*)src;

  // EVEN
  win = trgt.get_mpi_win();
  // Fence synchronization on the window
  MPI_Win_fence(MPI_MODE_NOSTORE, win);
  raw = (QUICQ_TYPE*)trgt.get_raw_arr();
  total_fps = trgt.get_data_placement()->get_n_local_arr_elems();
  // Copy data
  std::memcpy(raw, milc_raw_arr, total_fps*sizeof(QUICQ_TYPE));
  // data layout transformation when SIMD RTF is provided
  if(trgt.get_data_placement()->get_simd_dim_ext() > 1) {
    quicq::transform_to_rho_phi_layout_packedGauges(trgt);
  }
  MPI_Win_fence(0, win);
}

void getGauge (links_latt &src, su3_matrix *trgt)
{
  MPI_Win win;
  QUICQ_TYPE *raw;
  QUICQ_TYPE *milc_raw_arr;
  std::size_t total_fps;

  milc_raw_arr  = (QUICQ_TYPE*)trgt;

  win = src.get_mpi_win();
  // Fence synchronization on the window
  MPI_Win_fence(MPI_MODE_NOSTORE, win);
  // data layout transformation when SIMD RTF is provided
  if(src.get_data_placement()->get_simd_dim_ext() > 1) {
    quicq::transform_to_lexicographic_layout_packedGauges(src);
  }
  raw = (QUICQ_TYPE*)src.get_raw_arr();
  total_fps = src.get_data_placement()->get_n_local_arr_elems();
  // Copy data
  std::memcpy(milc_raw_arr, raw, total_fps*sizeof(QUICQ_TYPE));
  MPI_Win_fence(0, win);
}

void QUARC_ALWAYS_INLINE
ks_dslash_eo (ks_spinor_latt &o_su3v,
              const ks_spinor_latt &e_su3v,
              const links_latt &o_fl,
              const links_latt &o_ll)
{
  // KS Dslash read even sites write into odd sites
  o_su3v
    = DRILL<QUICQ_TUP>(o_fl) * e_su3v.GSHIFT<1,0,0,0>()
    + DRILL<QUICQ_ZUP>(o_fl) * e_su3v.GSHIFT<0,1,0,0>()
    + DRILL<QUICQ_YUP>(o_fl) * e_su3v.GSHIFT<0,0,1,0>()
    + DRILL<QUICQ_XUP>(o_fl) * IF_EVEN_CHOOSE(e_su3v.GSHIFT<0,0,0,1>(), e_su3v)

    + DRILL<QUICQ_TUP>(o_ll) * e_su3v.GSHIFT<3,0,0,0>()
    + DRILL<QUICQ_ZUP>(o_ll) * e_su3v.GSHIFT<0,3,0,0>()
    + DRILL<QUICQ_YUP>(o_ll) * e_su3v.GSHIFT<0,0,3,0>()
    + DRILL<QUICQ_XUP>(o_ll) * IF_EVEN_CHOOSE(e_su3v.GSHIFT<0,0,0,2>(),
                                              e_su3v.GSHIFT<0,0,0,1>())

    - adjoint(DRILL<QUICQ_TDOWN>(o_fl)) * e_su3v.GSHIFT<-1,0,0,0>()
    - adjoint(DRILL<QUICQ_ZDOWN>(o_fl)) * e_su3v.GSHIFT<0,-1,0,0>()
    - adjoint(DRILL<QUICQ_YDOWN>(o_fl)) * e_su3v.GSHIFT<0,0,-1,0>()
    - adjoint(DRILL<QUICQ_XDOWN>(o_fl)) *
      IF_EVEN_CHOOSE(e_su3v, e_su3v.GSHIFT<0,0,0,-1>())

    - adjoint(DRILL<QUICQ_TDOWN>(o_ll)) * e_su3v.GSHIFT<-3,0,0,0>()
    - adjoint(DRILL<QUICQ_ZDOWN>(o_ll)) * e_su3v.GSHIFT<0,-3,0,0>()
    - adjoint(DRILL<QUICQ_YDOWN>(o_ll)) * e_su3v.GSHIFT<0,0,-3,0>()
    - adjoint(DRILL<QUICQ_XDOWN>(o_ll)) *
      IF_EVEN_CHOOSE(e_su3v.GSHIFT<0,0,0,-1>(),
                     e_su3v.GSHIFT<0,0,0,-2>())
      ;
}

void QUARC_ALWAYS_INLINE
ks_dslash_oe (ks_spinor_latt &e_su3v,
              const ks_spinor_latt &o_su3v,
              const links_latt &e_fl,
              const links_latt &e_ll)
{
  // KS Dslash read odd sites write into even sites
  e_su3v
    = DRILL<QUICQ_TUP>(e_fl) * o_su3v.GSHIFT<1,0,0,0>() // t+1
    + DRILL<QUICQ_ZUP>(e_fl) * o_su3v.GSHIFT<0,1,0,0>() // z+1
    + DRILL<QUICQ_YUP>(e_fl) * o_su3v.GSHIFT<0,0,1,0>() // y+1
    + DRILL<QUICQ_XUP>(e_fl) * IF_EVEN_CHOOSE(o_su3v,
                                              o_su3v.GSHIFT<0,0,0,1>()) // x+1

    // su3Mul 3rd neighbors and the long links in forward directions
    + DRILL<QUICQ_TUP>(e_ll) * o_su3v.GSHIFT<3,0,0,0>() // t+3
    + DRILL<QUICQ_ZUP>(e_ll) * o_su3v.GSHIFT<0,3,0,0>() // z+3
    + DRILL<QUICQ_YUP>(e_ll) * o_su3v.GSHIFT<0,0,3,0>() // y+3
    + DRILL<QUICQ_XUP>(e_ll) * IF_EVEN_CHOOSE(o_su3v.GSHIFT<0,0,0,1>(),
                                              o_su3v.GSHIFT<0,0,0,2>())

    - adjoint(DRILL<QUICQ_TDOWN>(e_fl)) * o_su3v.GSHIFT<-1,0,0,0>()
    - adjoint(DRILL<QUICQ_ZDOWN>(e_fl)) * o_su3v.GSHIFT<0,-1,0,0>()
    - adjoint(DRILL<QUICQ_YDOWN>(e_fl)) * o_su3v.GSHIFT<0,0,-1,0>()
    - adjoint(DRILL<QUICQ_XDOWN>(e_fl)) *
      IF_EVEN_CHOOSE(o_su3v.GSHIFT<0,0,0,-1>(), o_su3v)

    // su3Mul 3rd neighbors and the long links in backward directions
    - adjoint(DRILL<QUICQ_TDOWN>(e_ll)) * o_su3v.GSHIFT<-3,0,0,0>()
    - adjoint(DRILL<QUICQ_ZDOWN>(e_ll)) * o_su3v.GSHIFT<0,-3,0,0>()
    - adjoint(DRILL<QUICQ_YDOWN>(e_ll)) * o_su3v.GSHIFT<0,0,-3,0>()
    - adjoint(DRILL<QUICQ_XDOWN>(e_ll)) *
      IF_EVEN_CHOOSE(o_su3v.GSHIFT<0,0,0,-2>(), o_su3v.GSHIFT<0,0,0,-1>())
    ;
}

} // end of anonymous namespace

void quicq_ks_dslash_eo_wrapper (milc_payload_t *mp)
{
#if DEBUG
  std::cout << "Create su3vector of global shape : ";
  for(int i = 0; i < ndims; ++i) {
    std::cout << gs_exts[i] << " ";
  }
  std::cout << '\n';
  std::cout << "Distribution RTFs : ";
  for(int i = 0; i < ndims; ++i) {
    std::cout << dist_rtfs[i] << " ";
  }
  std::cout << '\n';
#endif

  auto dp_spec = atl_json_spec_parser::parse("MILC-CG-ATL.json");
  auto mpi_ctx = quarc_mpi_context_create(mp->dist_rtfs, 0, 4,
                                          quarc_mpi_win_type::MPI3);

  // Define METL arrays to store the su3_vec and su3_mat
  GS gs(mp->gs_exts[0], mp->gs_exts[1], mp->gs_exts[2], mp->gs_exts[3]/2);
  ks_spinor_latt e_su3v(&gs,mpi_ctx,mp->dist_rtfs,dp_spec.simd_rtfs.data()),
                 o_su3v(&gs,mpi_ctx,mp->dist_rtfs,dp_spec.simd_rtfs.data());
  links_latt o_fl(&gs,mpi_ctx,mp->dist_rtfs,dp_spec.simd_rtfs.data()),
             o_ll(&gs,mpi_ctx,mp->dist_rtfs,dp_spec.simd_rtfs.data());

  // starting offset for the su3_vector
  auto in_su3_st = mp->src1;
  // starting offset for the MILC fat link su3_matrix array
  auto fl_st = mp->fat_links
             + (even_sites_on_node*QUICQ_NUM_PACKED_LINKS);
  // starting offset for the MILC long link su3_matrix array
  auto ll_st = mp->long_links
             + (even_sites_on_node*QUICQ_NUM_PACKED_LINKS);
  auto out_su3_v_st = mp->dest1 +  even_sites_on_node;

  // copy source su3_vectors from MILC to cb
  setKS(e_su3v, in_su3_st);
  // copy source su3_matrix from MILC to cb
  setGauge(o_fl, fl_st);
  setGauge(o_ll, ll_st);

  double dtime = -hrtimer_get();
  for(auto i = 0ul; i < NITERS; ++i) {
    //auto dtime = -hrtimer_get();
#if 1
    ks_dslash_eo(o_su3v, e_su3v, o_fl, o_ll);
#else
    // KS Dslash read even sites write into odd sites
    o_su3v
      = DRILL<QUICQ_TUP>(o_fl) * e_su3v.GSHIFT<1,0,0,0>()
      + DRILL<QUICQ_ZUP>(o_fl) * e_su3v.GSHIFT<0,1,0,0>()
      + DRILL<QUICQ_YUP>(o_fl) * e_su3v.GSHIFT<0,0,1,0>()
      + DRILL<QUICQ_XUP>(o_fl) * IF_EVEN_CHOOSE(e_su3v.GSHIFT<0,0,0,1>(), e_su3v)

      + DRILL<QUICQ_TUP>(o_ll) * e_su3v.GSHIFT<3,0,0,0>()
      + DRILL<QUICQ_ZUP>(o_ll) * e_su3v.GSHIFT<0,3,0,0>()
      + DRILL<QUICQ_YUP>(o_ll) * e_su3v.GSHIFT<0,0,3,0>()
      + DRILL<QUICQ_XUP>(o_ll) * IF_EVEN_CHOOSE(e_su3v.GSHIFT<0,0,0,2>(),
                                                e_su3v.GSHIFT<0,0,0,1>())

      - adjoint(DRILL<QUICQ_TDOWN>(o_fl)) * e_su3v.GSHIFT<-1,0,0,0>()
      - adjoint(DRILL<QUICQ_ZDOWN>(o_fl)) * e_su3v.GSHIFT<0,-1,0,0>()
      - adjoint(DRILL<QUICQ_YDOWN>(o_fl)) * e_su3v.GSHIFT<0,0,-1,0>()
      - adjoint(DRILL<QUICQ_XDOWN>(o_fl)) *
        IF_EVEN_CHOOSE(e_su3v, e_su3v.GSHIFT<0,0,0,-1>())

      - adjoint(DRILL<QUICQ_TDOWN>(o_ll)) * e_su3v.GSHIFT<-3,0,0,0>()
      - adjoint(DRILL<QUICQ_ZDOWN>(o_ll)) * e_su3v.GSHIFT<0,-3,0,0>()
      - adjoint(DRILL<QUICQ_YDOWN>(o_ll)) * e_su3v.GSHIFT<0,0,-3,0>()
      - adjoint(DRILL<QUICQ_XDOWN>(o_ll)) *
        IF_EVEN_CHOOSE(e_su3v.GSHIFT<0,0,0,-1>(),
                       e_su3v.GSHIFT<0,0,0,-2>())
        ;
#endif
    //dtime += hrtimer_get();
    //if(o_su3v.get_data_placement()->get_quarc_mpi_ctx()->rank == 0)
    //  std::cout << "Timing for iter " << i << " " << dtime << "(ns)\n";
  }
  dtime += hrtimer_get();
  if(o_su3v.get_data_placement()->get_quarc_mpi_ctx()->rank == 0)
    std::cout << "Timing for quicq_dslash_eo  " << NITERS << " iterations " 
              << dtime << "(s)\n";

  // copy data from cb to milc. Only su3_vectors need to be copied back
  getKS(o_su3v, out_su3_v_st);
}

void quicq_ks_dslash_oe_wrapper (milc_payload_t *mp)
{
#if DEBUG
  std::cout << "Create su3vector of global shape : ";
  for(int i = 0; i < ndims; ++i) {
    std::cout << gs_exts[i] << " ";
  }
  std::cout << '\n';
  std::cout << "Distribution RTFs : ";
  for(int i = 0; i < ndims; ++i) {
    std::cout << dist_rtfs[i] << " ";
  }
  std::cout << '\n';
#endif
  auto dp_spec = atl_json_spec_parser::parse("MILC-CG-ATL.json");
  auto mpi_ctx = quarc_mpi_context_create(mp->dist_rtfs, 0, 4,
                                          quarc_mpi_win_type::MPI3);
  // Define METL arrays to store the su3_vec and su3_mat
  GS gs(mp->gs_exts[0], mp->gs_exts[1], mp->gs_exts[2], mp->gs_exts[3]/2);
  ks_spinor_latt e_su3v(&gs,mpi_ctx,mp->dist_rtfs,dp_spec.simd_rtfs.data()),
                 o_su3v(&gs,mpi_ctx,mp->dist_rtfs,dp_spec.simd_rtfs.data());
  links_latt e_fl(&gs,mpi_ctx,mp->dist_rtfs,dp_spec.simd_rtfs.data()),
             e_ll(&gs,mpi_ctx,mp->dist_rtfs,dp_spec.simd_rtfs.data());

  // starting offset for the su3_vector
  auto in_su3_st = mp->src1 + even_sites_on_node;
  // starting offset for the MILC fat link su3_matrix array
  auto fl_st = mp->fat_links;
  // starting offset for the MILC long link su3_matrix array
  auto ll_st = mp->long_links;
  auto out_su3_v_st = mp->dest1;

  // copy source su3_vectors from MILC to cb
  setKS(o_su3v, in_su3_st);
  // copy source su3_matrix from MILC to cb
  setGauge(e_fl, fl_st);
  setGauge(e_ll, ll_st);

  auto dtime = -hrtimer_get();
  for(auto i = 0ul; i < NITERS; ++i) {
#if 1
    ks_dslash_oe(e_su3v, o_su3v, e_fl, e_ll);
#else
    // KS Dslash read odd sites write into even sites
    e_su3v
      = DRILL<QUICQ_TUP>(e_fl) * o_su3v.GSHIFT<1,0,0,0>() // t+1
      + DRILL<QUICQ_ZUP>(e_fl) * o_su3v.GSHIFT<0,1,0,0>() // z+1
      + DRILL<QUICQ_YUP>(e_fl) * o_su3v.GSHIFT<0,0,1,0>() // y+1
      + DRILL<QUICQ_XUP>(e_fl) * IF_EVEN_CHOOSE(o_su3v,
                                                o_su3v.GSHIFT<0,0,0,1>()) // x+1

      // su3Mul 3rd neighbors and the long links in forward directions
      + DRILL<QUICQ_TUP>(e_ll) * o_su3v.GSHIFT<3,0,0,0>() // t+3
      + DRILL<QUICQ_ZUP>(e_ll) * o_su3v.GSHIFT<0,3,0,0>() // z+3
      + DRILL<QUICQ_YUP>(e_ll) * o_su3v.GSHIFT<0,0,3,0>() // y+3
      + DRILL<QUICQ_XUP>(e_ll) * IF_EVEN_CHOOSE(o_su3v.GSHIFT<0,0,0,1>(),
                                                o_su3v.GSHIFT<0,0,0,2>())

      - adjoint(DRILL<QUICQ_TDOWN>(e_fl)) * o_su3v.GSHIFT<-1,0,0,0>()
      - adjoint(DRILL<QUICQ_ZDOWN>(e_fl)) * o_su3v.GSHIFT<0,-1,0,0>()
      - adjoint(DRILL<QUICQ_YDOWN>(e_fl)) * o_su3v.GSHIFT<0,0,-1,0>()
      - adjoint(DRILL<QUICQ_XDOWN>(e_fl)) *
        IF_EVEN_CHOOSE(o_su3v.GSHIFT<0,0,0,-1>(), o_su3v)

      // su3Mul 3rd neighbors and the long links in backward directions
      - adjoint(DRILL<QUICQ_TDOWN>(e_ll)) * o_su3v.GSHIFT<-3,0,0,0>()
      - adjoint(DRILL<QUICQ_ZDOWN>(e_ll)) * o_su3v.GSHIFT<0,-3,0,0>()
      - adjoint(DRILL<QUICQ_YDOWN>(e_ll)) * o_su3v.GSHIFT<0,0,-3,0>()
      - adjoint(DRILL<QUICQ_XDOWN>(e_ll)) *
        IF_EVEN_CHOOSE(o_su3v.GSHIFT<0,0,0,-2>(), o_su3v.GSHIFT<0,0,0,-1>())
      ;
#endif
  }
  dtime += hrtimer_get();
  if(e_su3v.get_data_placement()->get_quarc_mpi_ctx()->rank == 0)
     std::cout << "Timing for  " << NITERS << " iters  " << dtime << "(ns)\n";

  // copy data from cb to milc. Only su3_vectors need to be copied back
  getKS(e_su3v, out_su3_v_st);
}

Real quicq_magsq_su3vec_wrapper_eo (const milc_payload_t *mp)
{
  Real ret = 0.0;
  auto dp_spec = atl_json_spec_parser::parse("MILC-CG-ATL.json");
  auto mpi_ctx = quarc_mpi_context_create(mp->dist_rtfs, 0, 4,
                                          quarc_mpi_win_type::MPI3);
  // Define METL arrays to store the su3_vec and su3_mat
  GS gs(mp->gs_exts[0], mp->gs_exts[1],  mp->gs_exts[2], mp->gs_exts[3]/2);
  ks_spinor_latt e_su3v(&gs,mpi_ctx,mp->dist_rtfs,dp_spec.simd_rtfs.data());

  // starting offset for the su3_vector
  auto in_su3_st = mp->src1;
  // copy source su3_vectors from MILC to cb
  setKS(e_su3v, in_su3_st);

  auto dtime = -hrtimer_get();
  REDUCE(ret, mag_sq2(e_su3v), quarc::metl::add_op<Real,Real>);
  dtime += hrtimer_get();

  if(e_su3v.get_data_placement()->get_quarc_mpi_ctx()->rank == 0)
    std::cout << "Timing for quicq_magsq_su3vec " << dtime << "(ns)\n";

  return ret;
}

Real quicq_magsq_su3vec_wrapper_oe (const milc_payload_t *mp)
{
  Real ret = 0.0;
  // Define METL arrays to store the su3_vec and su3_mat
  GS gs(mp->gs_exts[0], mp->gs_exts[1], mp->gs_exts[2], mp->gs_exts[3]/2);
  auto dp_spec = atl_json_spec_parser::parse("MILC-CG-ATL.json");
  auto mpi_ctx = quarc_mpi_context_create(mp->dist_rtfs, 0, 4,
                                          quarc_mpi_win_type::MPI3);
  ks_spinor_latt o_su3v(&gs,mpi_ctx,mp->dist_rtfs,dp_spec.simd_rtfs.data());

  // starting offset for the su3_vector
  auto in_su3_st = mp->src1 + even_sites_on_node;
  // copy source su3_vectors from MILC to cb
  setKS(o_su3v, in_su3_st);

  auto dtime = -hrtimer_get();
  REDUCE(ret, mag_sq2(o_su3v), quarc::metl::add_op<Real,Real>);
  dtime += hrtimer_get();

  if(o_su3v.get_data_placement()->get_quarc_mpi_ctx()->rank == 0)
    std::cout << "Timing for quicq_magsq_su3vec " << dtime << "(ns)\n";

  return ret;
}

void quicq_axpy_wrapper_e (milc_payload_t *mp, Real A)
{
  // Define METL arrays to store the su3_vec and su3_mat
  GS gs(mp->gs_exts[0], mp->gs_exts[1], mp->gs_exts[2], mp->gs_exts[3]/2);

  auto dp_spec = atl_json_spec_parser::parse("MILC-CG-ATL.json");
  auto mpi_ctx = quarc_mpi_context_create(mp->dist_rtfs, 0, 4,
                                          quarc_mpi_win_type::MPI3);

  ks_spinor_latt o_su3v(&gs,mpi_ctx,mp->dist_rtfs,dp_spec.simd_rtfs.data());

  ks_spinor_latt X(&gs,mpi_ctx,mp->dist_rtfs,dp_spec.simd_rtfs.data());
  ks_spinor_latt Y(&gs,mpi_ctx,mp->dist_rtfs,dp_spec.simd_rtfs.data());
  ks_spinor_latt C(&gs,mpi_ctx,mp->dist_rtfs,dp_spec.simd_rtfs.data());

  // copy source su3_vectors from MILC to cb
  setKS(X, mp->src2);
  setKS(Y, mp->src1);

  auto axpy_time = -hrtimer_get();
  C = A*X + Y;
  axpy_time     += hrtimer_get();

  getKS(C, mp->dest1);

  if(X.get_data_placement()->get_quarc_mpi_ctx()->rank == 0)
    std::cout << "Timing for quicq_magsq_su3vec " << axpy_time << "(ns)\n";
}

void quicq_axpy_wrapper_o (milc_payload_t *mp, Real A)
{
  // Define METL arrays to store the su3_vec and su3_mat
  GS gs(mp->gs_exts[0], mp->gs_exts[1], mp->gs_exts[2], mp->gs_exts[3]/2);
  auto dp_spec = atl_json_spec_parser::parse("MILC-CG-ATL.json");
  auto mpi_ctx = quarc_mpi_context_create(mp->dist_rtfs, 0, 4,
                                          quarc_mpi_win_type::MPI3);
  ks_spinor_latt X(&gs,mpi_ctx,mp->dist_rtfs,dp_spec.simd_rtfs.data());
  ks_spinor_latt Y(&gs,mpi_ctx,mp->dist_rtfs,dp_spec.simd_rtfs.data());
  ks_spinor_latt C(&gs,mpi_ctx,mp->dist_rtfs,dp_spec.simd_rtfs.data());

  // copy source su3_vectors from MILC to cb
  setKS(X, mp->src2 + even_sites_on_node);
  setKS(Y, mp->src1 + even_sites_on_node);

  auto axpy_time = -hrtimer_get();
  C = A*X + Y;
  axpy_time     += hrtimer_get();

  getKS(C, mp->dest1 + even_sites_on_node);

  if(X.get_data_placement()->get_quarc_mpi_ctx()->rank == 0)
    std::cout << "Timing for quicq_magsq_su3vec " << axpy_time << "(ns)\n";
}

Real quicq_dot_product_wrapper_e (const milc_payload_t *mp)
{
  Real pkp = 0.0;
  auto dp_spec = atl_json_spec_parser::parse("MILC-CG-ATL.json");
  auto mpi_ctx = quarc_mpi_context_create(mp->dist_rtfs, 0, 4,
                                          quarc_mpi_win_type::MPI3);
  // Define METL arrays to store the su3_vec and su3_mat
  GS gs(mp->gs_exts[0], mp->gs_exts[1],
        mp->gs_exts[2], mp->gs_exts[3]/2);
  ks_spinor_latt X(&gs,mpi_ctx,mp->dist_rtfs,dp_spec.simd_rtfs.data());
  ks_spinor_latt Y(&gs,mpi_ctx,mp->dist_rtfs,dp_spec.simd_rtfs.data());

  // copy source su3_vectors from MILC to cb
  setKS(X, mp->src2);
  setKS(Y, mp->src1);

  auto dot_time = -hrtimer_get();
  REDUCE(pkp, dot2(Y, X), quarc::metl::add_op<Real,Real>);
  dot_time     += hrtimer_get();

  if(X.get_data_placement()->get_quarc_mpi_ctx()->rank == 0) {
    std::cout << "PKP : " << pkp << '\n';
    std::cout << "DOT TIME " << dot_time << '\n';
  }

  return pkp;
}

Real quicq_dot_product_wrapper_o (const milc_payload_t *mp)
{
  Real pkp = 0.0;
  auto dp_spec = atl_json_spec_parser::parse("MILC-CG-ATL.json");
  auto mpi_ctx = quarc_mpi_context_create(mp->dist_rtfs, 0, 4,
                                          quarc_mpi_win_type::MPI3);
  // Define METL arrays to store the su3_vec and su3_mat
  GS gs(mp->gs_exts[0], mp->gs_exts[1],
        mp->gs_exts[2], mp->gs_exts[3]/2);
  ks_spinor_latt X(&gs,mpi_ctx,mp->dist_rtfs,dp_spec.simd_rtfs.data());
  ks_spinor_latt Y(&gs,mpi_ctx,mp->dist_rtfs,dp_spec.simd_rtfs.data());

  // copy source su3_vectors from MILC to cb
  setKS(X, mp->src2 + even_sites_on_node);
  setKS(Y, mp->src1 + even_sites_on_node);

  auto dot_time = -hrtimer_get();
  REDUCE(pkp, dot2(Y, X), quarc::metl::add_op<Real,Real>);
  dot_time     += hrtimer_get();

  if(X.get_data_placement()->get_quarc_mpi_ctx()->rank == 0) {
    std::cout << "PKP : " << pkp << '\n';
    std::cout << "DOT TIME " << dot_time << '\n';
  }

  return pkp;
}

#if 0
void quicq_add_su3_vector_wrapper (milc_payload_t *milc_payload)
{

}

Real quicq_dot_product_wrapper (const milc_payload_t *milc_payload)
{

}

Real quicq_norm_wrapper (const milc_payload_t *milc_payload)
{

}
#endif
