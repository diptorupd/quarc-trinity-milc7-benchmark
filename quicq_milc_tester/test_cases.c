#include "test_cases.h"
#include "compare_fp_arrays.h"
#include "lattice.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

static const int PACKED_GAUGES_PER_SITE = 8;
static const int FP_PER_SU3MAT = 18;
static const int FP_PER_SU3VEC = 6;

bool compare_links_arrays (const su3_matrix *src1,
                           const su3_matrix *src2,
                           int parity,
                           int link_type)
{
  bool validation_result = true;
  int src_st_idx = 0, src_end_idx;
  const Real* milc_su3v_1 = (const Real*)src1;
  const Real* milc_su3v_2 = (const Real*)src2;

  assert(link_type == 1 || link_type == 2);

  if(parity == EVEN) {
    src_st_idx  = 0;
    src_end_idx = even_sites_on_node * PACKED_GAUGES_PER_SITE * FP_PER_SU3MAT;
  }
  else if (parity == ODD) {
    printf("compare_links_quicq_vs_milc ODD\n");
    src_st_idx  = even_sites_on_node * PACKED_GAUGES_PER_SITE * FP_PER_SU3MAT;
    src_end_idx = sites_on_node * PACKED_GAUGES_PER_SITE * FP_PER_SU3MAT;
  }
  else {
    fprintf(stderr,
            "Incorrect parity value. Should be either EVEN(2) or ODD(1).\n");
    exit(1);
  }

  for(; src_st_idx < src_end_idx; ++src_st_idx) {
#if 0
    std::cout << " A : " << m_arr1[milc_st_idx] << " E "
              << m_arr2[milc_st_idx] << '\n';
#endif
    bool test = compare_floats(milc_su3v_1[src_st_idx],milc_su3v_2[src_st_idx]);
    if(!test) {
      validation_result = test;
    }
  }
  return validation_result;
}

bool compare_su3vector_arrays (const su3_vector *src1,
                               const su3_vector *src2,
                               int parity)
{
  bool validation_result = true;
  int src_st_idx, src_end_idx;
  const Real* milc_su3v_1 = (const Real*)src1;
  const Real* milc_su3v_2 = (const Real*)src2;

  if(parity == EVEN) {
    src_st_idx  = 0;
    src_end_idx = even_sites_on_node * FP_PER_SU3VEC;
  }
  else if (parity == ODD)  {
    src_st_idx  = even_sites_on_node * FP_PER_SU3VEC;
    src_end_idx = sites_on_node * FP_PER_SU3VEC;
  }
  else {
    fprintf(stderr,
            "Incorrect parity value. Should be either EVEN(2) or ODD(1).\n");
    exit(1);
  }

  for(; src_st_idx < src_end_idx; ++src_st_idx) {
    bool test = compare_floats(milc_su3v_1[src_st_idx],milc_su3v_2[src_st_idx]);
    if(!test) {
      validation_result = test;
    }
  }

  return validation_result;
}

bool compare_reals (Real fp1, Real fp2)
{
  return compare_floats(fp1, fp2);
}

