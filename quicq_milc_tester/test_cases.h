#ifndef QUICQ_MILC_TEST_CASES_H_
#define QUICQ_MILC_TEST_CASES_H_

#include "../include/generic_ks.h"
#include <stdbool.h>

bool compare_links_arrays (const su3_matrix* src1,
                           const su3_matrix* src2,
                           int parity,
                           int link_type);

bool compare_su3vector_arrays (const su3_vector * src1,
                               const su3_vector * src2,
                               int parity);

bool compare_reals (Real fp1, Real fp2);

#endif /* QUICQ_MILC_TEST_CASES_H_ */
