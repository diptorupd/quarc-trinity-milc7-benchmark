#include "quicq_milc_payload_builder.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include "QUARC_wrapper.h"

static const int PACKED_GAUGES_PER_SITE = 8;
static const int FP_PER_SU3MAT = 18;
static const int FP_PER_SU3VEC = 6;

#define SET_UNITARY                 0

////////////////////////////// Private Helpers /////////////////////////////////

/// Copy of load_fatbacklinks (fermion_links_helpers.c), without the adjoint.
///
static su3_matrix* load_fatbacklinks_without_adjoint (ferm_links_t *fn)
{
  su3_matrix *t_fbl = NULL;
  su3_matrix *t_fl = fn->fat;
  su3_matrix *tempmat1 = NULL;
  msg_tag *tag[4];
  char myname[] = "load_fatbacklinks";

  // Allocate space for t_fbl if NULL
  t_fbl = (su3_matrix *)malloc(sites_on_node*4*sizeof(su3_matrix));
  if(t_fbl==NULL){
    printf("%s(%d): no room for t_fbl\n",myname,this_node);
    terminate(1);
  }
  
  tempmat1 = (su3_matrix *)malloc(sites_on_node*sizeof(su3_matrix));
  if(tempmat1 == NULL){
    printf("%s: Can't malloc temporary\n",myname);
    terminate(1);
  }
  
  // gather backwards fatlinks
  for(int dir=XUP; dir<=TUP; dir ++) {
    for (int i = 0; i < sites_on_node; i++) {
      tempmat1[i] = t_fl[dir+4*i];
    }
    tag[dir] = start_gather_field(tempmat1,
                                  sizeof(su3_matrix),
                                  OPP_DIR(dir),
                                  EVENANDODD,
                                  gen_pt[dir]);
    wait_gather( tag[dir] );
    for (int i = 0; i < sites_on_node; i++) {
      su3_matrix* temp_ = (t_fbl + dir + 4*i);
      *temp_ = *((su3_matrix *)gen_pt[dir][i]);
    }
    cleanup_gather( tag[dir] );
  }
  
  free(tempmat1); 
  tempmat1 = NULL;
  
  return t_fbl;
}

/// Copy of load_longbacklinks (fermion_links_helpers.c), without the adjoint.
///
static su3_matrix* load_longbacklinks_without_adjoint (ferm_links_t *fn)
{
  su3_matrix *t_lbl = NULL;
  su3_matrix *t_ll = fn->lng;
  su3_matrix *tempmat1 = NULL;
  msg_tag *tag[4];
  char myname[] = "load_longbacklinks";

  /// Allocate space for t_lbl if NULL
  t_lbl = (su3_matrix *)malloc(sites_on_node*4*sizeof(su3_matrix));
  if(t_lbl==NULL){
    printf("%s(%d): no room for t_lbl\n",myname,this_node);
    terminate(1);
  }
      
  tempmat1 = (su3_matrix *)malloc(sites_on_node*sizeof(su3_matrix));
  if(tempmat1 == NULL){
    printf("%s: Can't malloc temporary\n",myname);
    terminate(1);
  }
  
  /// gather backwards longlinks
  for(int dir=XUP; dir<=TUP; dir ++){
    for (int i = 0; i < sites_on_node; i++) {
      tempmat1[i] = t_ll[dir+4*i];
    }
    tag[dir] = start_gather_field(tempmat1,
                                  sizeof(su3_matrix),
                                  OPP_3_DIR(DIR3(dir)),
                                  EVENANDODD,
                                  gen_pt[dir]);
    wait_gather( tag[dir] );
    for (int i = 0; i < sites_on_node; i++) {
      su3_matrix * temp_ = (t_lbl + dir + 4*i);
      *temp_ = *((su3_matrix *)gen_pt[dir][i]);
    }
    cleanup_gather(tag[dir]);
  }
  
  free(tempmat1); 
  tempmat1 = NULL;
  
  return t_lbl;
}

#if SET_UNITARY
static void set1_su3vector(su3_vector *vec_, int site )
{
  for(int i = 0; i < 3; ++i) {
    vec_->c[i].real = 1.0f; //(float)site;
    vec_->c[i].imag = 0.0f;
  }
}

static void set0_su3vector(su3_vector *vec_, int site )
{
  for(int i = 0; i < 3; ++i) {
    vec_->c[i].real = 0.0f;
    vec_->c[i].imag = 0.0f;
  }
}

static void set1_su3matrix(su3_matrix *mat_, int dir, int site)
{
  int rows = 3, cols = 3;

  if(dir == TUP) {
  //if(dir == TUP || dir == ZUP || dir == YUP) {
  //if(dir == TUP || dir == ZUP || dir == YUP || dir == XUP) {
  //if(dir == XUP) {
#if 0
    for(int i = 0; i < rows; ++i) {
      for(int j = 0; j < cols; ++j) {
        //mat_->e[i][j].real = 0.0f;
        //mat_->e[i][j].imag = 0.0f;
      }
    }
#else
    mat_->e[0][0].real = 1.0f;
    mat_->e[0][0].imag = 0.0f;
    mat_->e[0][1].real = 0.0f;
    mat_->e[0][1].imag = 0.0f;
    mat_->e[0][2].real = 0.0f;
    mat_->e[0][2].imag = 0.0f;
    mat_->e[1][0].real = 0.0f;
    mat_->e[1][0].imag = 0.0f;
    mat_->e[1][1].real = 1.0f;
    mat_->e[1][1].imag = 0.0f;
    mat_->e[1][2].real = 0.0f;
    mat_->e[1][2].imag = 0.0f;
    mat_->e[2][0].real = 0.0f;
    mat_->e[2][0].imag = 0.0f;
    mat_->e[2][1].real = 0.0f;
    mat_->e[2][1].imag = 0.0f;
    mat_->e[2][2].real = 1.0f;
    mat_->e[2][2].imag = 0.0f;
#endif
  }
  else {
    for(int i = 0; i < rows; ++i) {
      for(int j = 0; j < cols; ++j) {
        mat_->e[i][j].real = 0.0f;
        mat_->e[i][j].imag = 0.0f;
      }
    }
  }
}

static void set0_su3matrix (su3_matrix *mat_)
{
  int rows = 3, cols = 3;
  for(int i = 0; i < rows; ++i) {
    for(int j = 0; j < cols; ++j) {
      mat_->e[i][j].real = 0.0f;
      mat_->e[i][j].imag = 0.0f;
    }
  }
}
#endif

/*!  \brief Initialize the lattice. 
 * 
 * Zero-out the dest (xxx) fermions and populate the "parity" half of the 
 * lattice randomly.
 */
static void init_milc_env()
{
#if SET_UNITARY
  int i;
  site *s;
#endif

  /* Zero out the destination su3_vectors */
  clear_latvec(F_OFFSET(xxx1), EVENANDODD);
  clear_latvec(F_OFFSET(xxx2), EVENANDODD);
  
  /* Load fat and long links for fermion measurements */
  load_ferm_links(&fn_links, &ks_act_paths);

  /* generate a pseudo-fermion configuration for the source_vectors */
  grsource_imp(F_OFFSET(phi1), mass1, EVENANDODD, &fn_links);
  grsource_imp(F_OFFSET(phi2), mass2, EVENANDODD, &fn_links);

#if SET_UNITARY
#if 0
  FORALLSITES(i,s){
    set1_su3vector((su3_vector *)F_PT(s,F_OFFSET(phi1)), i);
  }
#endif
  su3_matrix *t_ll = fn_links.lng;
  su3_matrix *t_fl = fn_links.fat;
  int dir;

#if 1
  // Debug fat links
  for (i = 0; i < sites_on_node; i++) {
    for(dir=XUP; dir<=TUP; dir++) {
      set1_su3matrix(t_fl + dir + 4*i, dir, i);
      //set0_su3matrix(t_fl + dir + 4*i);
    }
  }
#endif

#if 1
  // Debug long links
  for (i = 0; i < sites_on_node; i++) {
    for(dir=XUP; dir<=TUP; dir++){
      //set1_su3matrix(t_ll + dir + 4*i, dir, i);
      set0_su3matrix(t_ll + dir + 4*i);
    }
  }
#endif

#endif // end of SET_UNITARY
}

//////////////////////////// End of private helpers ////////////////////////////

/// Copy su3_vectors from the lattice site major array to a field major array.
su3_vector* get_su3_vectors_for_all_sites_on_node (field_offset ft)
{
  int i;
  site *s;
  su3_vector *tmp;

  if((tmp=(su3_vector *) malloc((sites_on_node)*sizeof(su3_vector))) == NULL) {
    printf("%s\n","ERROR: No room for temporaries");
    exit(1);
  }

  FORSOMEPARITY(i,s,EVENANDODD) {
    tmp[i] = *((su3_vector *)F_PT(s,ft));
  }

  return tmp;
}

su3_matrix* get_packed_fatlinks_for_all_sites_on_node (void)
{
  int i;
  site *s;
  int num_bytes;

  num_bytes = sizeof(su3_matrix)*sites_on_node*PACKED_GAUGES_PER_SITE;
  su3_matrix* packed = (su3_matrix*)(malloc(num_bytes));
  su3_matrix* t_fatlink = fn_links.fat;
  su3_matrix* t_fatback = load_fatbacklinks_without_adjoint(&fn_links);

  FORSOMEPARITY(i,s,EVENANDODD) {
    memcpy(&packed[i*8], &t_fatlink[4*i], 4*sizeof(su3_matrix));
    memcpy(&packed[i*8+4], &t_fatback[4*i], 4*sizeof(su3_matrix));
  }

  free(t_fatback);

  return packed;
}

su3_matrix* get_packed_longlinks_for_all_sites_on_node (void)
{
  int i;
  site *s;
  int num_bytes;

  num_bytes = sizeof(su3_matrix)*sites_on_node*PACKED_GAUGES_PER_SITE;
  su3_matrix* packed = (su3_matrix*)(malloc(num_bytes));

  su3_matrix* t_longlink = fn_links.lng;
  su3_matrix* t_longback = load_longbacklinks_without_adjoint(&fn_links);

  FORSOMEPARITY(i,s,EVENANDODD) {
    memcpy(&packed[i*8], &t_longlink[4*i], 4*sizeof(su3_matrix));
    memcpy(&packed[i*8+4], &t_longback[4*i], 4*sizeof(su3_matrix));
  }

  free(t_longback);

  return packed;
}

milc_payload_t* create_milc_env (void)
{
  milc_payload_t *me = (milc_payload_t*)(malloc(sizeof(milc_payload_t)));
  if(me == NULL) {
    fprintf(stderr, "Could not allocare milc_env\n");
    exit(1);
  }
  
  /// Init the milc env. Note this initializes all sites (even and odd)
  init_milc_env();
  
  // Get the blocking factors from MILC
  const int * ldims = get_logical_dimensions();
  me->dist_rtfs[0] = ldims[3];
  me->dist_rtfs[1] = ldims[2];
  me->dist_rtfs[2] = ldims[1];
  me->dist_rtfs[3] = ldims[0];

  me->gs_exts[0] = nt;
  me->gs_exts[1] = nz;
  me->gs_exts[2] = ny;
  me->gs_exts[3] = nx;
  node0_printf("Dist RTF %d %d %d %d\n",
               me->dist_rtfs[0], me->dist_rtfs[1], me->dist_rtfs[2],
               me->dist_rtfs[3]);
  // Get the global/local shape from MILC
  node0_printf("Global Shape %d %d %d %d\n", nt, nz, ny, nx);

  me->src1       = get_su3_vectors_for_all_sites_on_node(F_OFFSET(phi1));
  me->src2       = get_su3_vectors_for_all_sites_on_node(F_OFFSET(phi2));
  me->dest1      = get_su3_vectors_for_all_sites_on_node(F_OFFSET(xxx1));
  me->temp       = get_su3_vectors_for_all_sites_on_node(F_OFFSET(xxx1));
  me->fat_links  = get_packed_fatlinks_for_all_sites_on_node();
  me->long_links = get_packed_longlinks_for_all_sites_on_node();
  me->temp_links = (su3_matrix*)(malloc(sizeof(su3_matrix)*sites_on_node*8));

  assert(me->src1       != NULL);
  assert(me->src2       != NULL);
  assert(me->dest1      != NULL);
  assert(me->temp       != NULL);
  assert(me->fat_links  != NULL);
  assert(me->long_links != NULL);
  assert(me->temp_links != NULL);

  return me;
}

void destroy_milc_env (milc_payload_t *e)
{
  free(e->src1);
  free(e->src2);
  free(e->dest1);
  free(e->temp);
  free(e->fat_links);
  free(e->long_links);
  free(e->temp_links);
  free(e);
}

milc_cg_payload_t* create_milc_cg_payload (void)
{
  milc_cg_payload_t *cp=(milc_cg_payload_t*)(malloc(sizeof(milc_cg_payload_t)));
  if(cp == NULL) {
    fprintf(stderr, "Could not allocare milc_env\n");
    exit(1);
  }

  /// Init the milc env. Note this initializes all sites (even and odd)
  init_milc_env();

  // Get the blocking factors from MILC
  const int * ldims = get_logical_dimensions();
  cp->dist_rtfs[0] = ldims[3];
  cp->dist_rtfs[1] = ldims[2];
  cp->dist_rtfs[2] = ldims[1];
  cp->dist_rtfs[3] = ldims[0];

  cp->gs_exts[0] = nt;
  cp->gs_exts[1] = nz;
  cp->gs_exts[2] = ny;
  cp->gs_exts[3] = nx;
  node0_printf("Dist RTF %d %d %d %d\n",
               cp->dist_rtfs[0], cp->dist_rtfs[1], cp->dist_rtfs[2],
               cp->dist_rtfs[3]);
  // Get the global/local shape from MILC
  node0_printf("Global Shape %d %d %d %d\n", nt, nz, ny, nx);

  cp->src1        = get_su3_vectors_for_all_sites_on_node(F_OFFSET(phi1));
  cp->dest1       = get_su3_vectors_for_all_sites_on_node(F_OFFSET(xxx1));
  cp->fat_links  = get_packed_fatlinks_for_all_sites_on_node();
  cp->long_links = get_packed_longlinks_for_all_sites_on_node();

  assert(cp->src1       != NULL);
  assert(cp->dest1      != NULL);
  assert(cp->fat_links  != NULL);
  assert(cp->long_links != NULL);

  return cp;
}

void destroy_milc_cg_payload (milc_cg_payload_t *e)
{
  free(e->src1);
  free(e->dest1);
  free(e->fat_links);
  free(e->long_links);
  free(e);
}
