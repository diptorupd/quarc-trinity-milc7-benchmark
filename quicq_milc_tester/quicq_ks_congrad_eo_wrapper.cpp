#include "quicq_ks_congrad_eo_wrapper.h"
#include "quarc.hpp"
#include "METL/quarc_default_operators.hpp"
#include "quicq_ks_types.hpp"
#include "quicq_ks_operators.hpp"
#include "quicq_layout_transformers.hpp"
#include <err.h>

using namespace quarc::metl;
using namespace quicq;

static const char *prec_label[2] = {"F", "D"};

#define HRTIMER_MAX INT64_MAX
#define HRTIMER_MIN INT64_MIN
/* resolution of the hrtimer (nanoseconds) */
#define HRTICK 1000000000

namespace
{
constexpr size_t NITERS = 1;
using hrtimer_t = int64_t;
hrtimer_t hrtimer_get()
{
#ifdef CLOCK_REALTIME
  struct timespec now;
  hrtimer_t t;

  if(clock_gettime(CLOCK_REALTIME, &now) != 0) {
    warn("clock_gettime failed");
    return -1;
  }

  t = now.tv_sec;
  t *= 1000000000L;
  t += now.tv_nsec;
  return t;
#else
  struct timeval now;
  hrtimer_t t;

  if(gettimeofday(&now, NULL) == -1) {
    warn("gettimeofday failed");
    return -1;
  }

  t = now.tv_sec;
  t *= 1000000000L;
  t += now.tv_usec * 1000L;
  return t;
#endif
}

void setKS (ks_spinor_latt &trgt, const su3_vector *src)
{
  // Fence synchronization on the window
  MPI_Win_fence(MPI_MODE_NOSTORE, trgt.get_mpi_win());
  // get the underlying raw memory
  auto raw = (QUICQ_TYPE*)trgt.get_raw_arr();
  // pointer to the milc memory allocation
  auto *milc_raw_arr  = (const QUICQ_TYPE*)src;
  // Number of floating points in local mddarray blocks
  auto total_fps = trgt.get_data_placement()->get_n_local_arr_elems();
  // Copy data
  std::memcpy(raw, milc_raw_arr, total_fps*sizeof(QUICQ_TYPE));

  // data layout transformation to go here
  if(trgt.get_data_placement()->get_simd_dim_ext() > 1) {
    quicq::transform_to_rho_phi_layout_su3ate(trgt);
  }
  MPI_Win_fence(0, trgt.get_mpi_win());
}

void getKS (ks_spinor_latt &src, su3_vector *trgt)
{
  // pointer into the milc su3_vector array
  auto *milc_raw_arr  = (QUICQ_TYPE*)trgt;
  const QUICQ_TYPE* raw;
  MPI_Win win;
  size_t total_fps;

  win = src.get_mpi_win();
  raw = (const QUICQ_TYPE*)src.get_raw_arr();
  total_fps = src.get_data_placement()->get_n_local_arr_elems();
  // Fence synchronization on the window
  MPI_Win_fence(MPI_MODE_NOSTORE, win);
  // data layout transformation back to lexicographic
  if(src.get_data_placement()->get_simd_dim_ext() > 1) {
    quicq::transform_to_lexicographic_layout_su3ate(src);
  }
  // Copy data
  std::memcpy(milc_raw_arr, raw, total_fps*sizeof(QUICQ_TYPE));
  MPI_Win_fence(0, src.get_mpi_win());
}

void setGauge (links_latt &trgt, const su3_matrix *src)
{
  MPI_Win win;
  QUICQ_TYPE *raw;
  const QUICQ_TYPE *milc_raw_arr;
  std::size_t total_fps;

  milc_raw_arr  = (const QUICQ_TYPE*)src;

  // EVEN
  win = trgt.get_mpi_win();
  // Fence synchronization on the window
  MPI_Win_fence(MPI_MODE_NOSTORE, win);
  raw = (QUICQ_TYPE*)trgt.get_raw_arr();
  total_fps = trgt.get_data_placement()->get_n_local_arr_elems();
  // Copy data
  std::memcpy(raw, milc_raw_arr, total_fps*sizeof(QUICQ_TYPE));
  // data layout transformation when SIMD RTF is provided
  if(trgt.get_data_placement()->get_simd_dim_ext() > 1) {
    quicq::transform_to_rho_phi_layout_packedGauges(trgt);
  }
  MPI_Win_fence(0, win);
}

void getGauge (links_latt &src, su3_matrix *trgt)
{
  MPI_Win win;
  QUICQ_TYPE *raw;
  QUICQ_TYPE *milc_raw_arr;
  std::size_t total_fps;

  milc_raw_arr  = (QUICQ_TYPE*)trgt;

  win = src.get_mpi_win();
  // Fence synchronization on the window
  MPI_Win_fence(MPI_MODE_NOSTORE, win);
  // data layout transformation when SIMD RTF is provided
  if(src.get_data_placement()->get_simd_dim_ext() > 1) {
    quicq::transform_to_lexicographic_layout_packedGauges(src);
  }
  raw = (QUICQ_TYPE*)src.get_raw_arr();
  total_fps = src.get_data_placement()->get_n_local_arr_elems();
  // Copy data
  std::memcpy(milc_raw_arr, raw, total_fps*sizeof(QUICQ_TYPE));
  MPI_Win_fence(0, win);
}

void QUARC_ALWAYS_INLINE
ks_dslash_eo (ks_spinor_latt &o_su3v,
              const ks_spinor_latt &e_su3v,
              const links_latt &o_fl,
              const links_latt &o_ll)
{
  // KS Dslash read even sites write into odd sites
  o_su3v
    = DRILL<QUICQ_TUP>(o_fl) * e_su3v.GSHIFT<1,0,0,0>()
    + DRILL<QUICQ_ZUP>(o_fl) * e_su3v.GSHIFT<0,1,0,0>()
    + DRILL<QUICQ_YUP>(o_fl) * e_su3v.GSHIFT<0,0,1,0>()
    + DRILL<QUICQ_XUP>(o_fl) * IF_EVEN_CHOOSE(e_su3v.GSHIFT<0,0,0,1>(), e_su3v)

    + DRILL<QUICQ_TUP>(o_ll) * e_su3v.GSHIFT<3,0,0,0>()
    + DRILL<QUICQ_ZUP>(o_ll) * e_su3v.GSHIFT<0,3,0,0>()
    + DRILL<QUICQ_YUP>(o_ll) * e_su3v.GSHIFT<0,0,3,0>()
    + DRILL<QUICQ_XUP>(o_ll) * IF_EVEN_CHOOSE(e_su3v.GSHIFT<0,0,0,2>(),
                                              e_su3v.GSHIFT<0,0,0,1>())

    - adjoint(DRILL<QUICQ_TDOWN>(o_fl)) * e_su3v.GSHIFT<-1,0,0,0>()
    - adjoint(DRILL<QUICQ_ZDOWN>(o_fl)) * e_su3v.GSHIFT<0,-1,0,0>()
    - adjoint(DRILL<QUICQ_YDOWN>(o_fl)) * e_su3v.GSHIFT<0,0,-1,0>()
    - adjoint(DRILL<QUICQ_XDOWN>(o_fl)) *
      IF_EVEN_CHOOSE(e_su3v, e_su3v.GSHIFT<0,0,0,-1>())

    - adjoint(DRILL<QUICQ_TDOWN>(o_ll)) * e_su3v.GSHIFT<-3,0,0,0>()
    - adjoint(DRILL<QUICQ_ZDOWN>(o_ll)) * e_su3v.GSHIFT<0,-3,0,0>()
    - adjoint(DRILL<QUICQ_YDOWN>(o_ll)) * e_su3v.GSHIFT<0,0,-3,0>()
    - adjoint(DRILL<QUICQ_XDOWN>(o_ll)) *
      IF_EVEN_CHOOSE(e_su3v.GSHIFT<0,0,0,-1>(),
                     e_su3v.GSHIFT<0,0,0,-2>())
      ;
}

void QUARC_ALWAYS_INLINE
ks_dslash_oe (ks_spinor_latt &e_su3v,
              const ks_spinor_latt &o_su3v,
              const links_latt &e_fl,
              const links_latt &e_ll)
{
  // KS Dslash read odd sites write into even sites
  e_su3v
    = DRILL<QUICQ_TUP>(e_fl) * o_su3v.GSHIFT<1,0,0,0>() // t+1
    + DRILL<QUICQ_ZUP>(e_fl) * o_su3v.GSHIFT<0,1,0,0>() // z+1
    + DRILL<QUICQ_YUP>(e_fl) * o_su3v.GSHIFT<0,0,1,0>() // y+1
    + DRILL<QUICQ_XUP>(e_fl) * IF_EVEN_CHOOSE(o_su3v,
                                              o_su3v.GSHIFT<0,0,0,1>()) // x+1

    // su3Mul 3rd neighbors and the long links in forward directions
    + DRILL<QUICQ_TUP>(e_ll) * o_su3v.GSHIFT<3,0,0,0>() // t+3
    + DRILL<QUICQ_ZUP>(e_ll) * o_su3v.GSHIFT<0,3,0,0>() // z+3
    + DRILL<QUICQ_YUP>(e_ll) * o_su3v.GSHIFT<0,0,3,0>() // y+3
    + DRILL<QUICQ_XUP>(e_ll) * IF_EVEN_CHOOSE(o_su3v.GSHIFT<0,0,0,1>(),
                                              o_su3v.GSHIFT<0,0,0,2>())

    - adjoint(DRILL<QUICQ_TDOWN>(e_fl)) * o_su3v.GSHIFT<-1,0,0,0>()
    - adjoint(DRILL<QUICQ_ZDOWN>(e_fl)) * o_su3v.GSHIFT<0,-1,0,0>()
    - adjoint(DRILL<QUICQ_YDOWN>(e_fl)) * o_su3v.GSHIFT<0,0,-1,0>()
    - adjoint(DRILL<QUICQ_XDOWN>(e_fl)) *
      IF_EVEN_CHOOSE(o_su3v.GSHIFT<0,0,0,-1>(), o_su3v)

    // su3Mul 3rd neighbors and the long links in backward directions
    - adjoint(DRILL<QUICQ_TDOWN>(e_ll)) * o_su3v.GSHIFT<-3,0,0,0>()
    - adjoint(DRILL<QUICQ_ZDOWN>(e_ll)) * o_su3v.GSHIFT<0,-3,0,0>()
    - adjoint(DRILL<QUICQ_YDOWN>(e_ll)) * o_su3v.GSHIFT<0,0,-3,0>()
    - adjoint(DRILL<QUICQ_XDOWN>(e_ll)) *
      IF_EVEN_CHOOSE(o_su3v.GSHIFT<0,0,0,-2>(), o_su3v.GSHIFT<0,0,0,-1>())
    ;
}

/// CG Solver with METAL/QUICQ data types
///
int quicq_ks_cg_parity_eo (const quarc_rt::quarc_mpi_context *mpi_ctx,
                           quicq::ks_spinor_latt &e_dest,
                           quicq::ks_spinor_latt &e_ttt,
                           quicq::ks_spinor_latt &o_ttt,
                           quicq::ks_spinor_latt &e_resid,
                           quicq::ks_spinor_latt &e_cg_p,
                           const quicq::ks_spinor_latt &e_src,
                           const quicq::links_latt &o_fl,
                           const quicq::links_latt &e_fl,
                           const quicq::links_latt &o_ll,
                           const quicq::links_latt &e_ll,
                           quark_invert_control *qic,
                           Real mass)
{
  int max_restarts = qic->nrestart; /* maximum restarts */
  int max_cg = max_restarts*niter;  /* Maximum number of iterations */
  Real relrsqmin = qic->relresid;   /* desired relative residual (FNAL)*/
  Real a,b;                         /* Sugar's a,b */
  Real oldrsq;
  Real pkp = 0.0;                 /*last resid*2,pkp = cg_p.K.cg_p */
  Real source_norm = 0.0;
  Real msq_x4 = 4.0*mass*mass;
  Real rsqmin = qic->resid;
  auto nrestart  = 0;
  auto iteration = 0;
  Real rsq = 0.0, relrsq = 0; /* resid**2, rel resid*2 */
  qic->size_r    = 0;
  qic->size_relr = 0;
  double dtimec;
  double nflop = 1187;

  dtimec = -dclock();

  /////////////////////////// Start CG solver //////////////////////////////////

  auto my_rank = e_src.get_data_placement()->get_quarc_mpi_ctx()->rank;

  // Compute source norm
  REDUCE(source_norm, mag_sq2(e_src), quarc::metl::add_op<Real,Real>);
#ifdef CG_DEBUG
  if(my_rank == 0) {
    std::cout << "congrad: source_norm = " << (double)source_norm << '\n';
  }
#endif
  /* Start CG iterations */
  while(1) {
    /* Check for completion */
    if((iteration % niter == 0 ) ||
       ((rsqmin <= 0 || rsqmin > qic->size_r) &&
        (relrsqmin <= 0 || relrsqmin > qic->size_relr)))
    {
#ifdef CG_DEBUG
      if(my_rank==0) {
        std::cout << "(re)initialization process...\n";
      }
#endif
      rsq = 0.0;
      ks_dslash_eo(o_ttt, e_dest, o_fl, o_ll);
      MPI_Barrier(mpi_ctx->cart_comm);
      ks_dslash_oe(e_ttt, o_ttt, e_fl, e_ll);

      e_ttt = -msq_x4*e_dest + e_ttt;
      e_resid = e_src + e_ttt;
      e_cg_p  = e_src + e_ttt;

      REDUCE(rsq, mag_sq2(e_resid), quarc::metl::add_op<Real,Real>);
      qic->final_rsq    = (Real)rsq/source_norm;
      qic->final_relrsq = (Real)relrsq;
      iteration++ ;
      total_iters++;

#ifdef CG_DEBUG
      if(my_rank==0) {
        std::cout << "CONGRAD: (re)start rsq = " <<  qic->final_rsq
                  << " relrsq " << qic->final_relrsq << '\n';
      }
#endif

      if(iteration >= max_cg || nrestart  >= max_restarts ||
         ((rsqmin <= 0 || rsqmin > qic->final_rsq) &&
          (relrsqmin <= 0 || relrsqmin > qic->final_relrsq))) 
        break;
      nrestart++;
    }

    oldrsq = rsq;
    MPI_Barrier(mpi_ctx->cart_comm);
    ks_dslash_eo(o_ttt, e_cg_p, o_fl, o_ll);
    MPI_Barrier(mpi_ctx->cart_comm);
    ks_dslash_oe(e_ttt, o_ttt, e_fl, e_ll);

    pkp = 0.0;
    e_ttt = -msq_x4*e_cg_p + e_ttt;
    REDUCE(pkp, dot2(e_cg_p, e_ttt), quarc::metl::add_op<Real,Real>);
    
    iteration++;
    total_iters++;
    a = (Real)(-rsq/pkp);

    rsq = 0.0;
    e_dest  = a*e_cg_p + e_dest;
    e_resid = a*e_ttt  + e_resid;
    REDUCE(rsq, mag_sq2(e_resid), quarc::metl::add_op<Real,Real>);
    qic->size_r    = (Real)rsq/source_norm;
    qic->size_relr = (Real)relrsq;
#ifdef CG_DEBUG
    if(my_rank==0) {
      std::cout << "iter=" << iteration << " rsq/src=" << (double)qic->size_r
                << " relrsq=" <<  (double)qic->size_relr
                << " pkp=" << (double)pkp << '\n';
    }
#endif
    b = (Real)rsq/oldrsq;
    e_cg_p = b*e_cg_p + e_resid;
  } // while(1)

  dtimec += dclock();

  if(nrestart == max_restarts || iteration == max_cg) {
    if(my_rank==0) {
      std::cout << "ks_congrad: CG not converged after " << iteration << '\n';
      std::cout << "rsq. = " << qic->final_rsq << " wanted " << rsqmin
                << " relrsq = " << qic->final_relrsq << " wanted "
                << relrsqmin << '\n';
    }
  }
  
  if(my_rank==0) {
    std::cout << "QUICQ-CONGRAD5: time = " << dtimec
              << " Precision " << prec_label[PRECISION-1]
              << " masses = 1 "
              << " iters = " << iteration
              << " mflops = "
              << (double)(nflop*volume*iteration/(1.0e6*dtimec*numnodes()))
              << '\n';
  }

  return iteration;
}
} // end of anonymous namespace


int quicq_ks_cg_parity_eo_wrapper (milc_cg_payload_t *cgp,
                                   quark_invert_control *qic,
                                   Real mass)
{
  int iters = 0;
  auto dp_spec = quarc_rt::atl_json_spec_parser::parse("MILC-CG-ATL.json");
  auto mpi_ctx
  = quarc_rt::quarc_mpi_context_create(cgp->dist_rtfs, 0, 4,
                                       quarc_rt::quarc_mpi_win_type::MPI3);

  // Define METAL global shape for the arrays.
  GS gs(cgp->gs_exts[0], cgp->gs_exts[1], cgp->gs_exts[2], cgp->gs_exts[3]/2);

  // Allocate METAL arrays. The distribution is specified by MILC geometry. It
  // is best to use a fixed geometry. The simd reshape-transpose factors come
  // from the MILC-CG-ATL.json
  ks_spinor_latt e_src(&gs,mpi_ctx,cgp->dist_rtfs,dp_spec.simd_rtfs.data()),
                 e_dest(&gs,mpi_ctx,cgp->dist_rtfs,dp_spec.simd_rtfs.data()),
                 e_ttt(&gs,mpi_ctx,cgp->dist_rtfs,dp_spec.simd_rtfs.data()),
                 o_ttt(&gs,mpi_ctx,cgp->dist_rtfs,dp_spec.simd_rtfs.data()),
                 e_resid(&gs,mpi_ctx,cgp->dist_rtfs,dp_spec.simd_rtfs.data()),
                 e_cg_p(&gs,mpi_ctx,cgp->dist_rtfs,dp_spec.simd_rtfs.data())
                 ;
  links_latt o_fl(&gs,mpi_ctx,cgp->dist_rtfs,dp_spec.simd_rtfs.data()),
             o_ll(&gs,mpi_ctx,cgp->dist_rtfs,dp_spec.simd_rtfs.data()),
             e_fl(&gs,mpi_ctx,cgp->dist_rtfs,dp_spec.simd_rtfs.data()),
             e_ll(&gs,mpi_ctx,cgp->dist_rtfs,dp_spec.simd_rtfs.data())
             ;

  // starting offset for the MILC fat link su3_matrix array
  auto o_fl_st = cgp->fat_links + (even_sites_on_node*QUICQ_NUM_PACKED_LINKS);
  // starting offset for the MILC long link su3_matrix array
  auto o_ll_st = cgp->long_links + (even_sites_on_node*QUICQ_NUM_PACKED_LINKS);

  // copy source su3_matrix from MILC to QUICQ. The links are common across the
  // two CG solves.
  setGauge(o_fl, o_fl_st);
  setGauge(o_ll, o_ll_st);
  setGauge(e_fl, cgp->fat_links);
  setGauge(e_ll, cgp->long_links);

  // copy source su3_vectors from MILC to QUICQ
  setKS(e_src, cgp->src1);
  setKS(e_dest, cgp->dest1);

  iters += quicq_ks_cg_parity_eo(mpi_ctx, e_dest, e_ttt, o_ttt, e_resid,
                                 e_cg_p, e_src, o_fl, e_fl, o_ll, e_ll, qic,
                                 mass);
  // copy results from QUICQ to MILC.
  getKS(e_dest, cgp->dest1);

  return iters;
}

int quicq_ks_congrad_two_src (milc_cg_payload_t *cgp,
                              quark_invert_control *qic,
                              Real mass1, Real mass2,
                              Real *final_rsq)
{
  int iters = 0;
  auto dp_spec = quarc_rt::atl_json_spec_parser::parse("MILC-CG-ATL.json");
  auto mpi_ctx
  = quarc_rt::quarc_mpi_context_create(cgp->dist_rtfs, 0, 4,
                                       quarc_rt::quarc_mpi_win_type::MPI3);

  // Define METAL global shape for the arrays.
  GS gs(cgp->gs_exts[0], cgp->gs_exts[1], cgp->gs_exts[2], cgp->gs_exts[3]/2);

  // Allocate METAL arrays. The distribution is specified by MILC geometry. It
  // is best to use a fixed geometry. The simd reshape-transpose factors come
  // from the MILC-CG-ATL.json
  ks_spinor_latt e_src(&gs,mpi_ctx,cgp->dist_rtfs,dp_spec.simd_rtfs.data()),
                 e_dest(&gs,mpi_ctx,cgp->dist_rtfs,dp_spec.simd_rtfs.data()),
                 e_ttt(&gs,mpi_ctx,cgp->dist_rtfs,dp_spec.simd_rtfs.data()),
                 o_ttt(&gs,mpi_ctx,cgp->dist_rtfs,dp_spec.simd_rtfs.data()),
                 e_resid(&gs,mpi_ctx,cgp->dist_rtfs,dp_spec.simd_rtfs.data()),
                 e_cg_p(&gs,mpi_ctx,cgp->dist_rtfs,dp_spec.simd_rtfs.data())
                 ;
  links_latt o_fl(&gs,mpi_ctx,cgp->dist_rtfs,dp_spec.simd_rtfs.data()),
             o_ll(&gs,mpi_ctx,cgp->dist_rtfs,dp_spec.simd_rtfs.data()),
             e_fl(&gs,mpi_ctx,cgp->dist_rtfs,dp_spec.simd_rtfs.data()),
             e_ll(&gs,mpi_ctx,cgp->dist_rtfs,dp_spec.simd_rtfs.data())
             ;

  // starting offset for the MILC fat link su3_matrix array
  auto o_fl_st = cgp->fat_links + (even_sites_on_node*QUICQ_NUM_PACKED_LINKS);
  // starting offset for the MILC long link su3_matrix array
  auto o_ll_st = cgp->long_links + (even_sites_on_node*QUICQ_NUM_PACKED_LINKS);

  // copy source su3_matrix from MILC to QUICQ. The links are common across the
  // two CG solves.
  setGauge(o_fl, o_fl_st);
  setGauge(o_ll, o_ll_st);
  setGauge(e_fl, cgp->fat_links);
  setGauge(e_ll, cgp->long_links);

  //----- CG with mass1

  // copy source su3_vectors from MILC to QUICQ
  setKS(e_src, cgp->src1);
  setKS(e_dest, cgp->dest1);

  iters += quicq_ks_cg_parity_eo(mpi_ctx, e_dest, e_ttt, o_ttt, e_resid,
                                 e_cg_p, e_src, o_fl, e_fl, o_ll, e_ll, qic,
                                 mass1);
  // copy results from QUICQ to MILC.
  getKS(e_dest, cgp->dest1);

  //----- CG with mass2

  // copy source su3_vectors from MILC to QUICQ
  setKS(e_src, cgp->src2);
  setKS(e_dest, cgp->dest2);

  iters += quicq_ks_cg_parity_eo(mpi_ctx, e_dest, e_ttt, o_ttt, e_resid,
                                 e_cg_p, e_src, o_fl, e_fl, o_ll, e_ll, qic,
                                 mass2);
  // copy results from QUICQ to MILC.
  getKS(e_dest, cgp->dest2);

  /* Unpack the results */
  *final_rsq    = qic->final_rsq;

  quarc_rt::quarc_mpi_context_destroy(mpi_ctx);

  return iters;
}
