/* QUICQ-MILC Interface */
#ifndef QUICQ_MILC_PAYLOAD_BUILDER_H_
#define QUICQ_MILC_PAYLOAD_BUILDER_H_

#include "quicq_milc_payload.h"

su3_vector* get_su3_vectors_for_all_sites_on_node (field_offset ft);

su3_matrix* get_packed_fatlinks_for_all_sites_on_node (void);

su3_matrix* get_packed_longlinks_for_all_sites_on_node (void);

milc_payload_t* create_milc_env (void);

void destroy_milc_env (milc_payload_t *e);

milc_cg_payload_t* create_milc_cg_payload (void);

void destroy_milc_cg_payload (milc_cg_payload_t *e);

#endif /* QUICQ_MILC_PAYLOAD_BUILDER_H_ */
