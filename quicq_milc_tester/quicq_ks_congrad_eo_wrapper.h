#ifndef QUICQ_KS_CONGRAD_WRAPPER_H_
#define QUICQ_KS_CONGRAD_WRAPPER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "quicq_milc_payload.h"
#include "../include/generic_quark_types.h"

int quicq_ks_cg_parity_eo_wrapper (milc_cg_payload_t *milc_payload,
                                   quark_invert_control *qic,
                                   Real mass);

int quicq_ks_congrad_two_src (milc_cg_payload_t *cgp,
                              quark_invert_control *qic,
                              Real mass1, Real mass2,
                              Real *final_rsq);

#ifdef __cplusplus
}
#endif

#endif /* QUICQ_KS_CONGRAD_WRAPPER_H_ */
