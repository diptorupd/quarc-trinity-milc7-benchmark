#ifndef COMPARE_FP_ARRAYS_H_
#define COMPARE_FP_ARRAYS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "../include/precision.h"

bool compare_floats (Real fp1, Real fp2);

#ifdef __cplusplus
}
#endif

#endif /* COMPARE_FP_ARRAYS_H_ */
