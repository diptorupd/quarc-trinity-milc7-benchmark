#ifndef QUICQ_KS_OPERATORS_HPP_
#define QUICQ_KS_OPERATORS_HPP_

#include "quicq_config.h"
#include "quicq_ks_types.hpp"

//============================================================================//
//============================== elemental operators =========================//
//============================================================================//
namespace quicq
{

static inline auto su3_rdot_op (su3vector op1, su3vector op2)
{
  QUICQ_TYPE ret;
  
  ret  = op1[0][0] * op2[0][0];
  ret -= op1[0][1] * op2[0][1];
  ret += op1[1][0] * op2[1][0];
  ret -= op1[1][1] * op2[1][1];
  ret += op1[2][0] * op2[2][0];
  ret -= op1[2][1] * op2[2][1];
  
  return ret;
}

static inline auto su3_rdot_op2 (su3vector op1, su3vector op2)
{
  QUICQ_TYPE ret;
  
  ret  = op1[0][0] * op2[0][0];
  ret += op1[0][1] * op2[0][1];
  ret += op1[1][0] * op2[1][0];
  ret += op1[1][1] * op2[1][1];
  ret += op1[2][0] * op2[2][0];
  ret += op1[2][1] * op2[2][1];
  
  return ret;
}


/// @brief Addition of two SU(3) vectors.
///
static inline auto su3add_op (su3vector op1, su3vector op2)
{
  su3vector ret;

  ret[0][0] = op1[0][0] + op2[0][0];
  ret[0][1] = op1[0][1] + op2[0][1];

  ret[1][0] = op1[1][0] + op2[1][0];
  ret[1][1] = op1[1][1] + op2[1][1];

  ret[2][0] = op1[2][0] + op2[2][0];
  ret[2][1] = op1[2][1] + op2[2][1];

  return ret;
}

/// @brief Subtraction of two SU(3) vectors.
///
static inline auto su3subtract_op (su3vector op1, su3vector op2)
{
  su3vector ret;
  ret[0][0] = op1[0][0] - op2[0][0];
  ret[0][1] = op1[0][1] - op2[0][1];
      
  ret[1][0] = op1[1][0] - op2[1][0];
  ret[1][1] = op1[1][1] - op2[1][1];

  ret[2][0] = op1[2][0] - op2[2][0];
  ret[2][1] = op1[2][1] - op2[2][1];
  
  return ret;
}

static inline auto msq_su3_op (su3vector v1, su3vector v2)
{
  su3vector ret;
  
  ret[0][0] = v1[0][0] * v2[0][0];
  ret[0][1] = v1[0][1] * v2[0][1];
  ret[1][0] = v1[1][0] * v2[1][0];
  ret[1][1] = v1[1][1] * v2[1][1];
  ret[2][0] = v1[2][0] * v2[2][0];
  ret[2][1] = v1[2][1] * v2[2][1];
  
  return ret;
}

static inline auto msq_su3_op2 (su3vector v1)
{
  QUICQ_TYPE ret;
  
  ret  = v1[0][0] * v1[0][0];
  ret += v1[0][1] * v1[0][1];
  ret += v1[1][0] * v1[1][0];
  ret += v1[1][1] * v1[1][1];
  ret += v1[2][0] * v1[2][0];
  ret += v1[2][1] * v1[2][1];
  
  return ret;
}

static inline auto mult_su3_scalar_op (QUICQ_TYPE s, su3vector vec)
{
  su3vector ret;
  
  ret[0][0] = s*vec[0][0];
  ret[0][1] = s*vec[0][1];
  ret[1][0] = s*vec[1][0];
  ret[1][1] = s*vec[1][1];
  ret[2][0] = s*vec[2][0];
  ret[2][1] = s*vec[2][1];
  
  return ret;
}

/// @brief Multiply the SU(3) matrix by the SU(3) vector.
///
static inline auto su3mult_op (su3matrix mat, su3vector vec)
{
  su3vector ret;
  ret[0][0] = 0.0;
  ret[0][1] = 0.0;
  ret[1][0] = 0.0;
  ret[1][1] = 0.0;
  ret[2][0] = 0.0;
  ret[2][1] = 0.0;

  ret[0][0] += mat[0][0][0] * vec[0][0];
  ret[0][0] -= mat[0][0][1] * vec[0][1];
  ret[0][1] += mat[0][0][0] * vec[0][1];
  ret[0][1] += mat[0][0][1] * vec[0][0];

  ret[0][0] += mat[0][1][0] * vec[1][0];
  ret[0][0] -= mat[0][1][1] * vec[1][1];
  ret[0][1] += mat[0][1][0] * vec[1][1];
  ret[0][1] += mat[0][1][1] * vec[1][0];
  
  ret[0][0] += mat[0][2][0] * vec[2][0];
  ret[0][0] -= mat[0][2][1] * vec[2][1];
  ret[0][1] += mat[0][2][0] * vec[2][1];
  ret[0][1] += mat[0][2][1] * vec[2][0];
  
  ///
  
  ret[1][0] += mat[1][0][0] * vec[0][0];
  ret[1][0] -= mat[1][0][1] * vec[0][1];
  ret[1][1] += mat[1][0][0] * vec[0][1];
  ret[1][1] += mat[1][0][1] * vec[0][0];

  ret[1][0] += mat[1][1][0] * vec[1][0];
  ret[1][0] -= mat[1][1][1] * vec[1][1];
  ret[1][1] += mat[1][1][0] * vec[1][1];
  ret[1][1] += mat[1][1][1] * vec[1][0];
  
  ret[1][0] += mat[1][2][0] * vec[2][0];
  ret[1][0] -= mat[1][2][1] * vec[2][1];
  ret[1][1] += mat[1][2][0] * vec[2][1];
  ret[1][1] += mat[1][2][1] * vec[2][0];
  
  ///
  
  ret[2][0] += mat[2][0][0] * vec[0][0];
  ret[2][0] -= mat[2][0][1] * vec[0][1];
  ret[2][1] += mat[2][0][0] * vec[0][1];
  ret[2][1] += mat[2][0][1] * vec[0][0];

  ret[2][0] += mat[2][1][0] * vec[1][0];
  ret[2][0] -= mat[2][1][1] * vec[1][1];
  ret[2][1] += mat[2][1][0] * vec[1][1];
  ret[2][1] += mat[2][1][1] * vec[1][0];
  
  ret[2][0] += mat[2][2][0] * vec[2][0];
  ret[2][0] -= mat[2][2][1] * vec[2][1];
  ret[2][1] += mat[2][2][0] * vec[2][1];
  ret[2][1] += mat[2][2][1] * vec[2][0];

  return ret;
}

/// @brief Return the conjugate transpose of the su3matrix.
///
static inline auto conjugate_transpose_op (su3matrix mat)
{
  su3matrix ret;

  ret[0][0][0] = mat[0][0][0];
  ret[0][0][1] = -mat[0][0][1];

  ret[0][1][0] = mat[1][0][0];
  ret[0][1][1] = -mat[1][0][1];

  ret[0][2][0] = mat[2][0][0];
  ret[0][2][1] = -mat[2][0][1];

  ret[1][0][0] = mat[0][1][0];
  ret[1][0][1] = -mat[0][1][1];

  ret[1][1][0] = mat[1][1][0];
  ret[1][1][1] = -mat[1][1][1];

  ret[1][2][0] = mat[2][1][0];
  ret[1][2][1] = -mat[2][1][1];

  ret[2][0][0] = mat[0][2][0];
  ret[2][0][1] = -mat[0][2][1];

  ret[2][1][0] = mat[1][2][0];
  ret[2][1][1] = -mat[1][2][1];

  ret[2][2][0] = mat[2][2][0];
  ret[2][2][1] = -mat[2][2][1];

  return ret;
}
///@}

} // end of namespace quicq

//============================================================================//
//============================== Expression Templates ========================//
//============================================================================//

using namespace quicq;

/// The expression template operators are added to the quarc namespace to ensure
/// we can do compound assignments.
///
namespace quarc
{
namespace metl
{

static inline auto operator+ (su3vector op1, su3vector op2)
{
  return su3add_op (op1, op2);
}

static inline auto operator- (su3vector op1, su3vector op2)
{
  return su3subtract_op (op1, op2);
}

template <
  typename T1,
  typename std::enable_if <
    std::is_base_of<base_expression, T1>::value             &&
    std::is_same<su3vector, typename T1::value_type>::value
  >::type* = nullptr
>
const auto& mag_sq (const T1 & ref1)
{
  return
  expression_factory::binary_expr_builder<
    T1, T1, typename T1::value_type, msq_su3_op
  >(ref1, ref1);
}

template <
  typename T1,
  typename std::enable_if <
    std::is_base_of<base_expression, T1>::value             &&
    std::is_same<su3vector, typename T1::value_type>::value
  >::type* = nullptr
>
const auto& mag_sq2 (const T1 & ref1)
{
  return
  expression_factory::unary_expr_builder<
    T1, QUICQ_TYPE, msq_su3_op2
  >(ref1);
}

/// @brief Builds a binary expression encapsulating a su3matrix and su3vector
/// multiplication.
///
template <
  typename T1,
  typename T2,
  typename std::enable_if <
    std::is_base_of<base_expression, T1>::value &&
    std::is_base_of<base_expression, T2>::value &&
    std::is_same<su3vector, typename T2::value_type>::value
  >::type* = nullptr
>
const auto& operator* (const T1 & ref1, const T2 & ref2)
{
  return
  expression_factory::binary_expr_builder<
    T1, T2, typename T2::value_type, su3mult_op
  >(ref1, ref2);
}

/// @brief Overloaded operator*, that takes a scalar numeric value as the
/// left expression and su3vector lattice type as the rhs and returns
/// a binary_expression by encapsulating the mult_su3_scalar_op binary_op.
///
/// @param ref1
/// @param ref2
/// @return
///
template <
  typename T1,
  typename T2,
  typename std::enable_if <
    std::is_arithmetic<T1>::value               &&
    std::is_base_of<base_expression, T2>::value &&
    std::is_same<su3vector, typename T2::value_type>::value
  >::type* = nullptr
>
const auto&
operator* (const T1 &ref1, const T2 &ref2)
{
  return
  expression_factory::binary_expr_builder<
    scalar_terminal<T1>, T2, typename T2::value_type, mult_su3_scalar_op
  >(expression_factory::scalar_expr_builder<QUICQ_TYPE>(ref1), ref2);
}

/// @brief Builds a binary expression encapsulating the addition of two
/// su3vectors.
///
template <
  typename T1,
  typename T2,
  typename std::enable_if <
    std::is_base_of<base_expression, T1>::value &&
    std::is_base_of<base_expression, T2>::value &&
    std::is_same<su3vector, typename T2::value_type>::value
  >::type* = nullptr
>
const auto& operator+ (const T1 & ref1, const T2 & ref2)
{
  return
  expression_factory::binary_expr_builder<
    T1, T2, typename T2::value_type, su3add_op
  >(ref1, ref2);
}

/// @brief Builds a binary expression encapsulating the subtraction of two 
/// su3vectors.
///
template <
  typename T1,
  typename T2,
  typename std::enable_if <
    std::is_base_of<base_expression, T1>::value &&
    std::is_base_of<base_expression, T2>::value
  >::type* = nullptr
>
const auto& operator- (const T1 & ref1, const T2 & ref2)
{
  return
  expression_factory::binary_expr_builder<
    T1, T2, typename T2::value_type, su3subtract_op
  >(ref1, ref2);
}

/// @brief Builds a unary expression encapsulating the conjugate transpose of an
/// su3matrix.
///
template <
  typename T1,
  typename std::enable_if <
    std::is_base_of<base_expression, T1>::value
  >::type* = nullptr
>
const auto& adjoint (const T1 & ref1)
{
  return
  expression_factory::unary_expr_builder<
    T1, su3matrix, conjugate_transpose_op
  >(ref1);
}

/// @brief Compute dot product of two su3vectors.
///
template <
  typename T1,
  typename std::enable_if <
    std::is_base_of<base_expression, T1>::value &&
    std::is_same<su3vector, typename T1::value_type>::value
  >::type* = nullptr
>
const auto& dot (const T1 & ref1, const T1 & ref2)
{
  return
  expression_factory::binary_expr_builder<
    T1, T1, QUICQ_TYPE, su3_rdot_op
    >(ref1, ref2);
}

template <
  typename T1,
  typename std::enable_if <
    std::is_base_of<base_expression, T1>::value &&
    std::is_same<su3vector, typename T1::value_type>::value
  >::type* = nullptr
>
const auto& dot2 (const T1 & ref1, const T1 & ref2)
{
  return
  expression_factory::binary_expr_builder<
    T1, T1, QUICQ_TYPE, su3_rdot_op2
    >(ref1, ref2);
}


} // end of namespace metl
} // end of namespace quarc

#endif /* QUICQ_KS_OPERATORS_HPP_ */
