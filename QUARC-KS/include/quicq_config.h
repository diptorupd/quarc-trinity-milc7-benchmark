#ifndef QUICQ_CONFIG_H_
#define QUICQ_CONFIG_H_

#define QUICQ_FATLINK  1
#define QUICQ_LONGLINK 2

#ifndef PRECISION
#define PRECISION 1
#endif

#if PRECISION == 1
#define QUICQ_TYPE float
#else
#define QUICQ_TYPE double
#endif

/// Metadata about nested array types
#define QUICQ_NUM_FP_KS_SPINOR 6
#define QUICQ_NUM_FP_LINK      18
#define QUICQ_NUM_PACKED_LINKS 8

/// Parities (matched to how they are defined in MILC)
#define QUICQ_EVEN 2
#define QUICQ_ODD  1

/// Directions for the packed gauges
#define QUICQ_XUP   0
#define QUICQ_YUP   1
#define QUICQ_ZUP   2
#define QUICQ_TUP   3
#define QUICQ_XDOWN 4
#define QUICQ_YDOWN 5
#define QUICQ_ZDOWN 6
#define QUICQ_TDOWN 7

#endif /* QUICQ_CONFIG_H_ */
