#ifndef QUICQ_LAYOUT_TRANSFORMERS_HPP_
#define QUICQ_LAYOUT_TRANSFORMERS_HPP_

#include "quicq_config.h"
#include "quicq_ks_types.hpp"
#include <array>
#include <cassert>
#include <vector>

namespace quicq
{

template <typename T>
std::vector<std::size_t> get_simd_shape (const T &arr)
{
  std::vector<std::size_t> s;
  std::array<std::size_t,
             quarc::metl::num_nested_dims<typename T::value_type>::N> NExts;
  auto ls = arr.get_data_placement()->get_local_shape_with_simd();
  auto simd_rtfs = arr.get_simd_rtfs();

  // Append SIMD blocked extents
  for(auto i = 0ul; i < arr.RANK; ++i) {
    s.push_back(ls[i]);
  }

  // Append all nested dimensions
  quarc::metl::extract_nested_extents<typename T::value_type>(NExts,0);
  for(auto ne : NExts) {
    s.push_back(ne);
  }

  // Append SIMD dimensions
  for(auto i = 0; i < arr.RANK; ++i) {
    if(simd_rtfs[i] > 1) {
      s.push_back(simd_rtfs[i]);
    }
  }
#if 0
  std::cout << "SIMD Shape ";
  for(auto iv : s)
    std::cout << iv << " ";
  std::cout << '\n';
#endif
  return s;
}

template <typename T>
std::vector<std::size_t> get_strides (const T &arr)
{
  std::vector<std::size_t> IC;
  auto ls = get_simd_shape(arr);

  for(auto j = 0ul; j < ls.size(); ++j) {
    std::size_t tmp = 1;
    for(auto i = j+1; i < ls.size(); ++i) {
      tmp *= ls[i];
    }
    IC.push_back(tmp);
  }
  return IC;
}

static inline 
std::size_t
indexer (const std::vector<std::size_t> &IV,
         const std::vector<std::size_t> &ICV)
{
  std::size_t index = 0;
#if 0
  std::cout << "IV ";
  for(auto iv : IV)
    std::cout << iv << " ";
  std::cout << '\n';
#endif
#if 0
  std::cout << "ICV ";
  for(auto icv : ICV)
    std::cout << icv << " ";
  std::cout << '\n';
#endif
  assert(IV.size() == ICV.size() && "vector sizes do not match");
  for(auto i = 0ul; i < IV.size(); ++i) {
    index += IV[i]*ICV[i];
  }
  return index;
}

void transform_to_rho_phi_layout_su3ate (quicq::ks_spinor_latt &su3v)
{
  auto n_ic = get_strides(su3v);
  std::vector<std::size_t> iv;
  iv.reserve(n_ic.size());
  auto simd_rtf = su3v.get_simd_rtfs();
  auto BE0 = su3v.get_data_placement()->get_local_shape()[0];
  auto BE1 = su3v.get_data_placement()->get_local_shape()[1];
  auto BE2 = su3v.get_data_placement()->get_local_shape()[2];
  auto BE3 = su3v.get_data_placement()->get_local_shape()[3];

  auto SE0 = su3v.get_data_placement()->get_local_shape_with_simd()[0];
  auto SE1 = su3v.get_data_placement()->get_local_shape_with_simd()[1];
  auto SE2 = su3v.get_data_placement()->get_local_shape_with_simd()[2];
  auto SE3 = su3v.get_data_placement()->get_local_shape_with_simd()[3];

  // allocate a temporary array
  auto b_size = su3v.get_data_placement()->get_n_local_arr_elems();
  QUICQ_TYPE* tmp = new QUICQ_TYPE[b_size*sizeof(QUICQ_TYPE)];
  auto raw = (QUICQ_TYPE*)su3v.get_raw_arr();
  // Copy original data into tmp
  memcpy(tmp, raw, b_size*sizeof(QUICQ_TYPE));

  std::size_t iv_t0 = 0, iv_t1 = 0;
  std::size_t iv_z0 = 0, iv_z1 = 0;
  std::size_t iv_y0 = 0, iv_y1 = 0;
  std::size_t iv_x0 = 0, iv_x1 = 0;

  for(auto t = 0ul; t < BE0; ++t) {
    if(simd_rtf[0] != 1) {
      iv_t0 = t%SE0;
      iv_t1 = t/SE0;
    }
    else {
      iv_t0 = t;
    }
    for(auto z = 0ul; z < BE1; ++z) {
      if(simd_rtf[1] != 1) {
        iv_z0 = z%SE1;
        iv_z1 = z/SE1;
      }
      else {
        iv_z0 = z;
      }
      for(auto y = 0ul; y < BE2; ++y) {
        if(simd_rtf[2] != 1) {
          iv_y0 = y%SE2;
          iv_y1 = y/SE2;
        }
        else {
          iv_y0 = y;
        }
        for(auto x = 0ul; x < BE3; ++x) {
          if(simd_rtf[3] != 1) {
            iv_x0 = x%SE3;
            iv_x1 = x/SE3;
          }
          else {
            iv_x0 = x;
          }
          for(auto c = 0ul; c < 3; ++c) {
            for(auto r = 0ul; r < 2; ++r) {
              iv.push_back(iv_t0);
              iv.push_back(iv_z0);
              iv.push_back(iv_y0);
              iv.push_back(iv_x0);
              iv.push_back(c);
              iv.push_back(r);
              if(simd_rtf[0] != 1) {
                iv.push_back(iv_t1);
              }
              if(simd_rtf[1] != 1) {
                iv.push_back(iv_z1);
              }
              if(simd_rtf[2] != 1) {
                iv.push_back(iv_y1);
              }
              if(simd_rtf[3] != 1) {
                iv.push_back(iv_x1);
              }
              auto s_idx = t*BE1*BE2*BE3*3*2
                         + z*BE2*BE3*3*2
                         + y*BE3*3*2
                         + x*3*2
                         + c*2
                         + r;
              auto t_idx = indexer(iv, n_ic);
              raw[t_idx] = tmp[s_idx];
              iv.clear();
            }
          }
        }
      }
    }
  }
  // release tmp
  delete [] tmp;
}

void transform_to_lexicographic_layout_su3ate (quicq::ks_spinor_latt &su3v)
{
  auto n_ic = get_strides(su3v);
  std::vector<std::size_t> iv;
  iv.reserve(n_ic.size());
  auto simd_rtf = su3v.get_simd_rtfs();
  auto BE0 = su3v.get_data_placement()->get_local_shape()[0];
  auto BE1 = su3v.get_data_placement()->get_local_shape()[1];
  auto BE2 = su3v.get_data_placement()->get_local_shape()[2];
  auto BE3 = su3v.get_data_placement()->get_local_shape()[3];

  auto SE0 = su3v.get_data_placement()->get_local_shape_with_simd()[0];
  auto SE1 = su3v.get_data_placement()->get_local_shape_with_simd()[1];
  auto SE2 = su3v.get_data_placement()->get_local_shape_with_simd()[2];
  auto SE3 = su3v.get_data_placement()->get_local_shape_with_simd()[3];

  // allocate a temporary array
  auto b_size = su3v.get_data_placement()->get_n_local_arr_elems();
  QUICQ_TYPE* tmp = new QUICQ_TYPE[b_size*sizeof(QUICQ_TYPE)];
  auto raw = (QUICQ_TYPE*)su3v.get_raw_arr();
  // Copy original data into tmp
  memcpy(tmp, raw, b_size*sizeof(QUICQ_TYPE));

  std::size_t iv_t0 = 0, iv_t1 = 0;
  std::size_t iv_z0 = 0, iv_z1 = 0;
  std::size_t iv_y0 = 0, iv_y1 = 0;
  std::size_t iv_x0 = 0, iv_x1 = 0;

  for(auto t = 0ul; t < BE0; ++t) {
    if(simd_rtf[0] != 1) {
      iv_t0 = t%SE0;
      iv_t1 = t/SE0;
    }
    else {
      iv_t0 = t;
    }
    for(auto z = 0ul; z < BE1; ++z) {
      if(simd_rtf[1] != 1) {
        iv_z0 = z%SE1;
        iv_z1 = z/SE1;
      }
      else {
        iv_z0 = z;
      }
      for(auto y = 0ul; y < BE2; ++y) {
        if(simd_rtf[2] != 1) {
          iv_y0 = y%SE2;
          iv_y1 = y/SE2;
        }
        else {
          iv_y0 = y;
        }
        for(auto x = 0ul; x < BE3; ++x) {
          if(simd_rtf[3] != 1) {
            iv_x0 = x%SE3;
            iv_x1 = x/SE3;
          }
          else {
            iv_x0 = x;
          }
          for(auto c = 0ul; c < 3; ++c) {
            for(auto r = 0ul; r < 2; ++r) {
              iv.push_back(iv_t0);
              iv.push_back(iv_z0);
              iv.push_back(iv_y0);
              iv.push_back(iv_x0);
              iv.push_back(c);
              iv.push_back(r);
              if(simd_rtf[0] != 1) {
                iv.push_back(iv_t1);
              }
              if(simd_rtf[1] != 1) {
                iv.push_back(iv_z1);
              }
              if(simd_rtf[2] != 1) {
                iv.push_back(iv_y1);
              }
              if(simd_rtf[3] != 1) {
                iv.push_back(iv_x1);
              }
              auto s_idx = t*BE1*BE2*BE3*3*2
                         + z*BE2*BE3*3*2
                         + y*BE3*3*2
                         + x*3*2
                         + c*2
                         + r;
              auto t_idx = indexer(iv, n_ic);
              raw[s_idx] = tmp[t_idx];
              iv.clear();
            }
          }
        }
      }
    }
  }
  // release tmp
  delete [] tmp;
}

void
transform_to_rho_phi_layout_packedGauges (quicq::links_latt &su3m)
{
  auto n_ic = get_strides(su3m);
  std::vector<std::size_t> iv;
  iv.reserve(n_ic.size());
  auto simd_rtf = su3m.get_simd_rtfs();
  auto BE0 = su3m.get_data_placement()->get_local_shape()[0];
  auto BE1 = su3m.get_data_placement()->get_local_shape()[1];
  auto BE2 = su3m.get_data_placement()->get_local_shape()[2];
  auto BE3 = su3m.get_data_placement()->get_local_shape()[3];

  auto SE0 = su3m.get_data_placement()->get_local_shape_with_simd()[0];
  auto SE1 = su3m.get_data_placement()->get_local_shape_with_simd()[1];
  auto SE2 = su3m.get_data_placement()->get_local_shape_with_simd()[2];
  auto SE3 = su3m.get_data_placement()->get_local_shape_with_simd()[3];

  // allocate a temporary array
  auto b_size = su3m.get_data_placement()->get_n_local_arr_elems();
  QUICQ_TYPE* tmp = new QUICQ_TYPE[b_size*sizeof(QUICQ_TYPE)];
  auto raw = (QUICQ_TYPE*)su3m.get_raw_arr();
  // Copy original data into tmp
  memcpy(tmp, raw, b_size*sizeof(QUICQ_TYPE));

  std::size_t iv_t0 = 0, iv_t1 = 0;
  std::size_t iv_z0 = 0, iv_z1 = 0;
  std::size_t iv_y0 = 0, iv_y1 = 0;
  std::size_t iv_x0 = 0, iv_x1 = 0;

  for(auto t = 0ul; t < BE0; ++t) {
    if(simd_rtf[0] != 1) {
      iv_t0 = t%SE0;
      iv_t1 = t/SE0;
    }
    else {
      iv_t0 = t;
    }
    for(auto z = 0ul; z < BE1; ++z) {
      if(simd_rtf[1] != 1) {
        iv_z0 = z%SE1;
        iv_z1 = z/SE1;
      }
      else {
        iv_z0 = z;
      }
      for(auto y = 0ul; y < BE2; ++y) {
        if(simd_rtf[2] != 1) {
          iv_y0 = y%SE2;
          iv_y1 = y/SE2;
        }
        else {
          iv_y0 = y;
        }
        for(auto x = 0ul; x < BE3; ++x) {
          if(simd_rtf[3] != 1) {
            iv_x0 = x%SE3;
            iv_x1 = x/SE3;
          }
          else {
            iv_x0 = x;
          }
          for(auto p = 0ul; p < 8; ++p) {
            for(auto r = 0ul; r < 3; ++r) {
              for(auto c = 0ul; c < 3; ++c) {
                for(auto re = 0ul; re < 2; ++re) {
                  iv.push_back(iv_t0);
                  iv.push_back(iv_z0);
                  iv.push_back(iv_y0);
                  iv.push_back(iv_x0);
                  iv.push_back(p);
                  iv.push_back(r);
                  iv.push_back(c);
                  iv.push_back(re);
                  if(simd_rtf[0] != 1) {
                    iv.push_back(iv_t1);
                  }
                  if(simd_rtf[1] != 1) {
                    iv.push_back(iv_z1);
                  }
                  if(simd_rtf[2] != 1) {
                    iv.push_back(iv_y1);
                  }
                  if(simd_rtf[3] != 1) {
                    iv.push_back(iv_x1);
                  }
                  auto s_idx = t*BE1*BE2*BE3*8*3*3*2
                             + z*BE2*BE3*8*3*3*2
                             + y*BE3*8*3*3*2
                             + x*8*3*3*2
                             + p*3*3*2
                             + r*3*2
                             + c*2
                             + re;
                  auto t_idx = indexer(iv, n_ic);
                  raw[t_idx] = tmp[s_idx];
                  iv.clear();
                }
              }
            }
          }
        }
      }
    }
  }
  // release tmp
  delete [] tmp;
}

void
transform_to_lexicographic_layout_packedGauges (quicq::links_latt &su3m)
{
  auto n_ic = get_strides(su3m);
  std::vector<std::size_t> iv;
  iv.reserve(n_ic.size());
  auto simd_rtf = su3m.get_simd_rtfs();
  auto BE0 = su3m.get_data_placement()->get_local_shape()[0];
  auto BE1 = su3m.get_data_placement()->get_local_shape()[1];
  auto BE2 = su3m.get_data_placement()->get_local_shape()[2];
  auto BE3 = su3m.get_data_placement()->get_local_shape()[3];

  auto SE0 = su3m.get_data_placement()->get_local_shape_with_simd()[0];
  auto SE1 = su3m.get_data_placement()->get_local_shape_with_simd()[1];
  auto SE2 = su3m.get_data_placement()->get_local_shape_with_simd()[2];
  auto SE3 = su3m.get_data_placement()->get_local_shape_with_simd()[3];

  // allocate a temporary array
  auto b_size = su3m.get_data_placement()->get_n_local_arr_elems();
  QUICQ_TYPE* tmp = new QUICQ_TYPE[b_size*sizeof(QUICQ_TYPE)];
  auto raw = (QUICQ_TYPE*)su3m.get_raw_arr();
  // Copy original data into tmp
  memcpy(tmp, raw, b_size*sizeof(QUICQ_TYPE));

  std::size_t iv_t0 = 0, iv_t1 = 0;
  std::size_t iv_z0 = 0, iv_z1 = 0;
  std::size_t iv_y0 = 0, iv_y1 = 0;
  std::size_t iv_x0 = 0, iv_x1 = 0;

  for(auto t = 0ul; t < BE0; ++t) {
    if(simd_rtf[0] != 1) {
      iv_t0 = t%SE0;
      iv_t1 = t/SE0;
    }
    else {
      iv_t0 = t;
    }
    for(auto z = 0ul; z < BE1; ++z) {
      if(simd_rtf[1] != 1) {
        iv_z0 = z%SE1;
        iv_z1 = z/SE1;
      }
      else {
        iv_z0 = z;
      }
      for(auto y = 0ul; y < BE2; ++y) {
        if(simd_rtf[2] != 1) {
          iv_y0 = y%SE2;
          iv_y1 = y/SE2;
        }
        else {
          iv_y0 = y;
        }
        for(auto x = 0ul; x < BE3; ++x) {
          if(simd_rtf[3] != 1) {
            iv_x0 = x%SE3;
            iv_x1 = x/SE3;
          }
          else {
            iv_x0 = x;
          }
          for(auto p = 0ul; p < 8; ++p) {
            for(auto r = 0ul; r < 3; ++r) {
              for(auto c = 0ul; c < 3; ++c) {
                for(auto re = 0ul; re < 2; ++re) {
                  iv.push_back(iv_t0);
                  iv.push_back(iv_z0);
                  iv.push_back(iv_y0);
                  iv.push_back(iv_x0);
                  iv.push_back(p);
                  iv.push_back(r);
                  iv.push_back(c);
                  iv.push_back(re);
                  if(simd_rtf[0] != 1) {
                    iv.push_back(iv_t1);
                  }
                  if(simd_rtf[1] != 1) {
                    iv.push_back(iv_z1);
                  }
                  if(simd_rtf[2] != 1) {
                    iv.push_back(iv_y1);
                  }
                  if(simd_rtf[3] != 1) {
                    iv.push_back(iv_x1);
                  }
                  auto s_idx = t*BE1*BE2*BE3*8*3*3*2
                             + z*BE2*BE3*8*3*3*2
                             + y*BE3*8*3*3*2
                             + x*8*3*3*2
                             + p*3*3*2
                             + r*3*2
                             + c*2
                             + re;
                  auto t_idx = indexer(iv, n_ic);
                  raw[s_idx] = tmp[t_idx];
                  iv.clear();
                }
              }
            }
          }
        }
      }
    }
  }
  // release tmp
  delete [] tmp;
}
}

#endif /* QUICQ_LAYOUT_TRANSFORMERS_HPP_ */
