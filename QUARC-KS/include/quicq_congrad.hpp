#ifndef QUICQ_CONGRAD_HPP_
#define QUICQ_CONGRAD_HPP_

#ifdef __cplusplus
extern "C" {
#endif

void* create_ks_lattice (const int* gs_exts,
                         const int* dist_rtfs,
                         const int* simd_rtfs,
                         int ndims);

void destroy_ks_lattice (void * cb_lattice_ptr);

void setKS (void *cb_ks, const void *su3_v, int parity);

void getKS (void *cb_ks, void *su3_v, int parity);

void setGauge (void *cb_ks, const void *links, int parity, int link_type);

void ks_dslash_wrapper (void *cb_ks, int outputparity);

#ifdef __cplusplus
}
#endif

#endif /* QUICQ_CONGRAD_HPP_ */
