#ifndef QUICQ_KS_TYPES_HPP_
#define QUICQ_KS_TYPES_HPP_

#include "METL/quarc_mddarray.hpp"
#include "METL/quarc_sdlarray.hpp"
#include "METL/quarc_global_shape.hpp"
#include "quicq_config.h"              /* QUICQ_TYPE */

namespace quicq
{
using qcomplex           = quarc::metl::sdlarray<QUICQ_TYPE, 2>;
/// A three-dimensional vector of complex numbers
using su3vector          = quarc::metl::sdlarray<qcomplex, 3>;

/// A 3x3 matrix formed out of three su3vectors
using su3matrix          = quarc::metl::sdlarray<su3vector, 3>;
/// A packed array of eight SU3 matrices
using packedGauges       = quarc::metl::sdlarray<su3matrix, 8>;

using GS                 = quarc::metl::global_shape<4>;

/// Lattice types
using ks_spinor_latt     = quarc::metl::mddarray<su3vector, GS>;
using links_latt         = quarc::metl::mddarray<packedGauges, GS>;

} // end of namespace quicq

#endif /* QUICQ_KS_TYPES_HPP_ */
